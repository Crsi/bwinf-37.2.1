\section{Implementierung}

Die Implementierung wurde in Python 3 angefertigt. Das Programm wurde ausgiebig unter Python 3.5.3 mit Linux (Debian 64-bit) getestet. Abgesehen von der Standardbibliothek sind keine weiteren Pakete zwingend erforderlich.

Es kann allerdings eine Fortschrittsanzeige aktiviert werden. Dazu ist das Paket \texttt{tqdm} nötig. Zum Installieren von \texttt{tqdm} kann folgendes Kommando verwendet werden:

\begin{center}
	\ovalbox{\texttt{pip3 install tqdm}}
\end{center}

Das Vorhandensein dieser Bibliothek hat keinen Einfluss auf die Funktion des Programms oder Lösung des Problems -- es ist vollkommen optional.

\subsection{Modulübersicht}

Die folgende Tabelle gibt eine Übersicht über die Module, die im Rahmen der Implementierung angefertigt worden sind. Dabei ist \texttt{main.py} nicht aufgeführt, da es kein Modul, sondern ein Skript zur reinen Nutzerinteraktion, ist.

\begin{table}[h!]
	\begin{tabular}{l|l}
		Modulname & Bedeutung \\
		\hline
		\texttt{constants} & Konfiguration der Projektkonstanten \\
		\texttt{edge} & Definition der Kanten \\
		\texttt{environment} & Definition der Umgebung / Welt \\
		\texttt{generator} & Definition des Bild- und Weltgenerators \\
		\texttt{graph} & Definition des Graphen \\
		\texttt{log} & Bereitstellung der Logging-Funktionen \\
		\texttt{polygon} & Definition der Polygone \\
		\texttt{processor} & Definition des Prozessors \\
		\texttt{reader} & Funktionen zum Einlesen der Dateien \\
		\texttt{vertices} & Definition der Knoten \\
		\texttt{visibility} & Funktionen zum Prüfen der Sichtbarkeit \\
	\end{tabular}
\end{table}
\label{tab:module-overview}

Alle Dateien hier haben die Dateiendung \texttt{.py} (Python-Modul). Es werden allerdings zusätzlich auch Dateien mit Endung \texttt{.pyi} (Python-Stub) mitgeliefert. Bei der Nutzung von IDEs und höheren Tools mit \emph{statischen Codecheckern} sind diese Dateien von Nutzen, da sie zwar lediglich die Interfaces und Datentypen beschreiben (nicht implementieren), aber den Checker damit informieren, ob der verfasste Code korrekt ist.

% Formatting
\newpage

\subsection{Datenstrukturen}

\subsubsection{Vertex}

Die ,,niedrigsten`` Daten sind die einzelnen Punkte des Graphen. Die dafür zuständige Klasse \texttt{Vertex} (Modul \texttt{vertices}) speichert folgende Attribute:

\begin{table}[h!]
	\begin{tabular}{lc|l}
		Attribut & Typ & Bedeutung\\
		\hline
		\texttt{x} & \texttt{int} & x-Koordinate des Knotens \\
		\texttt{y} & \texttt{int} & y-Koordinate des Knotens \\
		\texttt{free} & \texttt{bool} & Angabe, ob der Knoten als ,,frei`` markiert ist \\
		\texttt{id} & \texttt{int} & ID des Polygons dieses Knotens \\
	\end{tabular}
	\label{tab:implementation-vertex-attributes}
\end{table}

Es werden ausschließlich Methoden für die Nutzung der Daten des \texttt{Vertex} bereitgestellt (beispielsweise Zugriff über Indizes oder Vergleichsoperatoren), aber sonst sind keine weiteren Methoden vorhanden.

\subsubsection{Edge}

Tatsächlich ist die Kantenstruktur \texttt{Edge} (Modul \texttt{edge}) eine veraltete Struktur. Frühere Versionen nutzten sie zur Darstellung der Adjazenzmatrix, nun wird sie für die einfachere Gewichtung eines Knoten (Scoring) und zur Feststellung von Schnittpunkten genutzt.

Denn zu diesem Zweck wurde die Methode \texttt{hits} implementiert, die angibt, ob ein übergebener Knoten auf der Kante liegt -- dabei wird impliziert, dass der Knoten bereits auf der \emph{Geraden} der Kante liegt. Das heißt, es wird nun nur noch bestätigt, ob ein Knoten auch auf der \emph{Strecke} (Kante) liegt. Diese Methode wird zum Feststellen des Schnittpunkts zweier Kanten genutzt.

\subsubsection{Polygon}

Ein Hindernis des Graphen wird mit dem Typ \texttt{Polygon} dargestellt. Statt dieses Datentyps hätte eine leicht modifizierte \texttt{list} den Dienst auch getan, denn abgesehen von Methoden für den Zugriff auf die Attribute finden sich beim \texttt{Polygon} nur zwei Datenwerte: eine Liste aller Knoten, die das Polygon definieren sowie eine im Graphen eindeutige Polygon-ID. Diese wird mit \texttt{-1} initialisiert. Dabei ist das auch gleichzeitig der Spezialwert für \emph{nicht definiert}. Ein Knoten ohne Polygon (beispielsweise der Startpunkt) hat demnach auch \texttt{-1} als ID. Im Graphen sollte jedes Polygon aufsteigend eine solche ID bekommen.

\subsubsection{Graph}
\label{subsubsec:graph-implementation}

Der Graph ist eine Datenstruktur, die dazu bestimmt ist, die anderen zugrundeliegenden Daten des Graphen (und Visibility Graphs) zu verwalten. Zu diesem Zweck wird ein Graph mit einer Liste von Polygonen initialisiert, die so in dem Attribut \texttt{polygons} gespeichert und in ein assoziatives Array \texttt{adjacency} übertragen werden. Schlüssel hierfür sind die Knoten (\texttt{Vertex}), während eine Menge aller Knoten, die mit dem Schlüssel eine Kante bilden, die Werte zu diesem Schlüssel sind.

In einem Objekt der Klasse \texttt{Graph} wird sowohl der ,,Startgraph`` als auch der Visibility Graph verwaltet. Für Letzteren gibt es ein als \texttt{blocker} bezeichnetes assoziatives Array mit dem gleichen Aufbau wie \texttt{adjacency}. Die Werte in der Menge für einen Schlüssel geben hier allerdings diejenigen Knoten an, die \textbf{nicht} erreicht werden können.

% Formatting
\newpage

Der \texttt{Graph} stellt komfortable Methoden für den Zugriff auf die gespeicherten Daten -- so etwa mit den Operatoren \texttt{+} und \texttt{--}, Zugriff über Indizes oder die Nutzung von \texttt{in} -- bereit. Eine Methode \texttt{get} gibt die Elemente eines angegebenen Typs zurück, mit \texttt{free} werden alle \emph{freien} Knoten bestimmt, mit \texttt{next} werden alle von einem Knoten erreichbaren Knoten zurückgegeben und mit \texttt{verify} kann die Aktualität\footnote{Siehe auch Abschnitt \ref{subsubsec:path-finding}} einer Kante überprüft werden.

\subsubsection{Environment}

Als eine ,,Umgebung`` wird ein Objekt der Klasse \texttt{Environment} gesehen. Eine Umgebung ist dafür zuständig, den Graphen in dieser Welt genauso wie den gefundenen Pfad, den Startpunkt und die Größe der Welt zu speichern. Dazu wird beim Schritt 3 der Verarbeitung (siehe Abschnitt \ref{itm:main-steps}) diese Umgebung erzeugt. Auch der \texttt{Reader} gibt eine Umgebung zurück.

Das Modul \texttt{environment} definiert neben der Klasse auch zwei Funktionen \texttt{load} und \texttt{save}, die für die persistente Speicherung von Umgebungen in Dateien zuständig wären. Hintergrund ist, dass der Großteil der Rechenzeit in die Erstellung des Visibility Graphs fließt. Dieser wird auch in der Umgebung gespeichert. Durch Wiederverwendung eines bereits erstellten Visibility Graphs kann erheblich Rechenzeit gespart werden!

Die Klasse selbst stellt im Wesentlichen die Methode \texttt{build} bereit. Einziger optionaler Parameter ist die Angabe, ob der Fortschrittsbalken verwendet werden soll oder nicht. \texttt{build} baut aus dem gespeicherten Graphen den Visibility Graph (dabei werden beide eigentlich getrennten Graphen im gleichen Attribut gespeichert, siehe dazu Abschnitt \ref{subsubsec:graph-implementation}).

Dazu wird eine Liste von zu prüfenden Knoten verwaltet, in die nacheinander alle bereits bearbeiteten Knoten hinzugefügt werden. So muss in jedem Schritt der aktuelle Knoten gegen die $i-1$ vorherigen Knoten geprüft werden und nicht gegen alle $|V|$ Knoten des Graphen. In jedem Schritt wird dabei für einen Knoten geprüft, ob er ,,frei`` ist. Anschließend wird für alle $i-1$ deren Sichtbarkeit überprüft: ist sie nicht gegeben, so wird die entsprechende Kante blockiert.

Dass dieses Vorgehen mit der Prüfung der vorherigen Knoten überhaupt möglich ist, wird durch die Sortierung der zu durchlaufenden Knoten sichergestellt. Der Grundgedanke dabei ist, dass eine Kante, die vollständig ,,hinter`` dem zu prüfenden Knoten ist, nicht geprüft werden muss, da sie keine Schnittpunkte haben kann.

\subsubsection{Processor}

Die Klasse \texttt{Processor} stellt die nötige Funktionalität bereit, um einen Pfad durch den Visibility Graph zu finden. Im Einzelnen ist das eine lokal in \texttt{Processor.nextStep} definierte Funktion \texttt{f} zum Bewerten eines Knotens (mehr in Abschnitt \ref{subsubsec:vertex-scoring}). Dabei ist \texttt{nextStep} die Methode, die hauptsächlich verantwortlich für das Finden eines Pfads ist (siehe Abschnitt \ref{subsubsec:path-finding}).

Wie in Abschnitt \ref{subsubsec:auto-optimization} näher erläutert wird, gibt es auch eine Methode \texttt{optimize}, deren Zweck die Verkürzung einer bereits gefundenen Route ist (dies wird besonders für Beispiel 5 benötigt -- siehe Abschnitt \ref{tab:results-overview}).

Der \texttt{Processor} speichert überdies eine Referenz auf die Umgebung (\texttt{Environment}), auf die er angewendet wird, genauso wie eine Referenz auf die Parameter (Typ \texttt{argparse.Namespace}), mit denen das Programm gestartet worden ist, und eine Referenz auf den Bildgenerator, der verwendet werden soll. Die Klassenmethode \texttt{calc} berechnet die Länge eines angegebenen Pfades.

Aufgerufen wird vor allem die Hauptmethode des Prozessors: \texttt{main}. Diese Methode erwartet keine Parameter und gibt auch ,,nur`` zurück, ob die Suche erfolgreich war. Der eigentliche Pfad wird im Attribut \texttt{path} des \texttt{Environment}-Objekts gespeichert. \texttt{main} lässt zuerst den Visibility Graph generieren, dann startet auch schon die Suche nach dem kürzesten bzw. besten Pfad. Die Methode \texttt{nextStep} wird erst mit einem Limit von \texttt{0} aufgerufen (es darf niemals nach ,,hinten`` gegangen werden). Erst dann, und nur dann, wenn kein Pfad gefunden werden konnte, wird dieses Limit immer wieder um eins erhöht, bis ein Pfad gefunden werden konnte -- oder \texttt{PATHFINDER\_LIMIT}\footnote{Siehe Abschnitt \ref{subsubsec:configuration}} erreicht ist.

Anschließend wird der gefundene Pfad gemäß \texttt{Processor.optimize} optimiert\footnote{Siehe Abschnitt \ref{subsubsec:auto-optimization}}. Danach wird die maximale Größe der Umgebung in dessen Attribut \texttt{size} bestimmt -- insbesondere für die Generierung der SVG-Ausgaben ist dies sehr wichtig. Zuletzt wird der boolesche Rückgabewert zurückgegeben, ob ein Pfad gefunden werden konnte.

\subsection{Algorithmen}

\subsubsection{Berechnen von Schnittpunkten}
\label{subsubsec:calculation-intersection}

Die Funktion \texttt{visibility.find} findet für zwei Kanten \texttt{e1} und \texttt{e2} den Schnittpunkt dieser beiden Kanten. Dazu wird die Formel zur Schnittpunktberechnung wie in Abschnitt \ref{subsubsec:intersection-calculation} verwendet. Bei der Nutzung dieser Formel kann ein \texttt{ZeroDivisionError} auftreten, dann gibt es keinen gemeinsamen Schnittpunkt.

Zudem können berechnete Schnittpunkte außerhalb der beiden Kanten liegen, da bei der Formel ausschließlich Koordinaten verwendet werden. Um diesen Fall abzudecken, wird anschließend noch die Methode \texttt{Edge.hits} aufgerufen.

Die Funktion gibt ein Tuple mit zwei Fließkommazahlen als Koordinaten oder \texttt{None}, wenn es keinen Schnittpunkt gibt, zurück. Es wird ein Tuple und kein \texttt{Vertex} als Rückgabewert verwendet, da ein \texttt{Vertex} mit ganzen Zahlen definiert worden ist. Außerdem wird damit der Fakt, dass der Punkt nicht zum Graphen gehört, besser veranschaulicht. Schließlich prüfen Funktionen, die \texttt{visibility.find} aufrufen, den Rückgabewert auf \texttt{None}.

\texttt{visibility.findIntersection} ist ein Wrapper zum Aufrufen von \texttt{find}.

\subsubsection{Sichtbarkeitsprüfung}

Ebenfalls im Modul \texttt{visibility} befindet sich die Funktion \texttt{isVisible}, welche für einen gegebenen Start- und einen Endknoten zusammen mit dem Graphen, in dem sich die beiden befinden, aufgerufen wird. Zurückgegeben wird die Information, ob Start- und Endpunkt mit einer freien Sichtlinie verbunden sind.

Dazu wird jede Kante von jedem Knoten des Graphen mit der Kante (Start, Ende) verglichen. Sobald ein Schnittpunkt gefunden wird, gibt die Funktion \texttt{False} zurück. Sollten alle Kanten abgearbeitet sein, so gibt sie \texttt{True} zurück: Start- und Endknoten können sich sehen!

% Formatting
\newpage

\subsubsection{Scoring von Knoten}
\label{subsubsec:vertex-scoring}

Der lokalen Funktion \texttt{f} innerhalb von \texttt{Processor.nextStep} wird ein Knoten übergeben. Dafür wird ein Score für diesen Knoten zurückgegeben. \texttt{f} nutzt dabei die Variable \texttt{start} des übergeordneten Namensraums als Vergleichswert.

Der Score\footnote{Siehe auch Abschnitt \ref{subsubsec:theory-scoring}.} wird durch Multiplizieren eines vorher berechneten Wertes \texttt{smart} mit der Summe aus den beiden Bewertungen berechnet. Diese beiden Werte sind einerseits der Abstand des betrachteten Knoten zur y-Achse mit dem Vorfaktor \texttt{SCORE\_X} und andererseits die Distanz zwischen dem betrachteten Knoten und dem aktuellen Startknoten mit dem Vorfaktor \texttt{SCORE\_DISTANCE}.

Der Wert von \texttt{smart} ist entweder der von \texttt{SCORE\_UPWARDS} oder der von \texttt{SCORE\_DOWNWARDS}. Der erste Fall tritt in einem von drei Fällen ein:

\begin{itemize}
	\item Beide x-Werte sind gleich und der y-Wert von \texttt{smart} ist kleiner oder gleich
	\item Die Steigung $m$ der Geraden durch den aktuellen Knoten und den Startknoten ist positiv und der betrachtete Knoten weiter ,,rechts`` als der Startknoten
	\item Die Steigung $m$ der Geraden durch den aktuellen Knoten und den Startknoten ist negativ und der betrachtete Knoten weiter ,,links`` als der Startknoten
\end{itemize}

Andernfalls ist \texttt{smart} stets gleich \texttt{SCORE\_DOWNWARDS}. Die Relationen ,,links`` bzw. ,,rechts`` beziehen sich auf den Wert der x-Koordinate des Punktes.

\subsubsection{Routenfindung}
\label{subsubsec:path-finding}

Die bereits angesprochene Methode \texttt{nextStep} der Klasse \texttt{Processor} soll schrittweise den Pfad zum Ziel -- der y-Achse -- finden und zurück liefern. Die Methode erwartet einen Startknoten \texttt{start}, der Teil des Graphen \texttt{graph} ist, sowie die Angabe, wie oft die Optimierung, nur in die richtige Richtung zu gehen, ignoriert werden darf. Zudem kann ein Zielpunkt \texttt{destination} angegebenen werden, der in diesem Projekt aber stets \texttt{None} ist.

Zuerst wird geprüft, ob der Zielpunkt bzw. ein ,,freier`` Knoten erreicht worden ist und in diesem Fall die Methode gleich mit den entsprechenden Rückgabewerten beendet. Ansonsten wird eine leere Liste \texttt{targets} angelegt, in der alle Knoten, die vom aktuellen \texttt{start} erreicht werden können, gesammelt werden. Ob ein Knoten \texttt{node} erreicht werden kann, wird durch den Ausdruck \texttt{graph.check((start, node)) and graph.verify(start, node)} geprüft. Dabei gibt der Aufruf von \texttt{check} an, ob die angegebene Kante nicht blockiert ist, während der Aufruf von \texttt{verify} bestätigt, dass die Kante nicht durch ein Polygon hindurch geht.

Anschließend werden die Zielknoten \emph{in-place} gemäß der Bewertungsfunktion \texttt{f}, wie in Abschnitt \ref{subsubsec:vertex-scoring} erläutert, sortiert. Nun wird \texttt{nextStep} für jeden Knoten dieser sortierten Liste rekursiv aufgerufen (und dabei lediglich noch der Wert von \texttt{escape} um 1 reduziert).

Wenn der zurückgegebene Pfad tatsächlich Elemente enthält, also nicht leer ist, und das erste Element des Pfads die y-Achse berührt, so wird an den Pfad der derzeitige Startknoten angehängt und dieses Konstrukt zurückgegeben. Sollte kein einziger Knoten erfolgreich aufgelöst werden können, so wird eine leere Liste zurückgegeben, denn dann wurde kein Pfad gefunden.

% Formatting
\newpage

\subsubsection{Automatische Optimierungen}
\label{subsubsec:auto-optimization}

Die Methode \texttt{optimize} der Klasse \texttt{Processor} stellt ein nützliches Feature bereit, welches den Pfad optimiert. Der Grundgedanke ist, dass teils nicht optimale (um mehrere Ecken gehende) Routen vom Pfadfinder geliefert werden. Nun werden diese Ecken, die ausgelassen werden könnten, schlicht übersprungen. Das Resultat ist ein insgesamt kürzerer Weg.

Der Start- und Endpunkt eines Pfades wird nicht betrachtet, aber für jeden Knoten dazwischen wird geprüft, ob man ihn überspringen (und damit den Umstand aus Abschnitt \ref{subsubsec:shortest-path-general} nutzen) könnte. Dazu werden zwei gegenläufige Zähler \texttt{i} und \texttt{e} zum Zugriff auf die übergebene Liste von Knoten \texttt{path} verwendet. Eine zweite Liste \texttt{final} speichert währenddessen alle Knoten, die garantiert im Resultat, also im verkürzten Pfad, enthalten sind. Hinzugefügt wird nun der erste Knoten \texttt{path[e]}, der vom letzten Knoten \texttt{path[i]} sichtbar ist, dann wird i inkrementiert und e zurückgesetzt.

Das Resultat ist eine Liste von Knoten, die keine ,,unnötigen`` Umwege mehr enthält, soweit dies eben möglich ist.

\subsection{Sonstiges}

\subsubsection{Skript zur Nutzerinteraktion}

Das Skript \texttt{main.py} dient als vollständige Kommandozeilenschnittstelle zum Nutzer. Es exportiert keine direkt für die Lösung des Problems notwendige Funktionalität und erhielt daher auch keine \texttt{.pyi}-Datei. Weitere Informationen zur Nutzung des Programms und insbesondere dieses Skripts finden sich im folgenden Abschnitt bei den Beispielen.

Intern wird beim Aufrufen von \texttt{main.py} eine Hauptfunktion \texttt{main} mit den Kommandozeilenparametern \texttt{sys.argv} aufgerufen. Eine Hilfsfunktion \texttt{\_setup} kümmert sich dann unter Verwendung des exzellenten Moduls \texttt{argparse} um die Erstellung der korrekten Schnittstelle.

Die Funktion bearbeitet im Wesentlichen die folgenden Schritte:

\begin{enumerate}
	\item Erstellen der Kommandozeilenschnittstelle
	\item Prüfen der Eingaben
	\item Einlesen der Datei bzw. Erstellen einer zufälligen Karte (Bereitstellung eines \texttt{Environment})
	\item Ausführen des Prozessors auf die Umgebung
	\item Generieren von Ausgaben (z. B. SVG-Grafiken)
	\item Ausgeben der Resultate
\end{enumerate}
\label{itm:main-steps}

\subsubsection{Einlesen von Eingaben}

Das Modul \texttt{reader} stellt eine Klasse \texttt{Reader} bereit, welche fürs Einlesen von Textdateien im Format wie \texttt{lisarennt}\textbf{?}\texttt{.txt} zuständig ist. Dabei wird \texttt{Reader.read} verwendet, welches einen Pfad und die Kodierung (hier stets \texttt{UTF-8}) als Eingabe erhält und ein Objekt der Klasse \texttt{Environment} zurück liefert. Intern wird der Inhalt der Datei an \texttt{Reader.extract} weitergereicht.

Zusammen mit \texttt{Reader.create} sorgt \texttt{extract} für die Zerlegung der einzelnen Zeilen in Polygone aus \texttt{Vertex}-Objekten und die Zusammenstellung eines \texttt{Environment}-Objekts. Dabei werden außerdem ausgiebige Tests auf die Eingabe angewendet, sodass ein Nutzer auch eine nützliche Rückmeldung über Fehler in einer Eingabedatei erhält.

% Formatting
\newpage

\subsubsection{Weitergehende Programmkonfiguration}
\label{subsubsec:configuration}

Das Modul \texttt{constants} ist ausgelagert worden, sodass ein Nutzer diese Datei unkompliziert bearbeiten kann. Dabei sollte sich natürlich an die Syntax von Python 3 gehalten werden.

Mögliche Änderungen in der Datei wären beispielsweise die Änderung der Geschwindigkeit von Lisa oder des Busses -- ein schneller fahrender Bus und eine langsamere Lisa sind realistisch gesehen ohnehin wahrscheinlicher.

Aber ein interessanter Aspekt ist auch die Änderung der Scoring-Vorfaktoren mit den Bezeichnungen \texttt{SCORE\_} usw. Damit ließe sich das Programm beispielsweise sagen, dass es bevorzugt dem Bus entgegen gehen sollte (\texttt{SCORE\_DOWNWARDS} bzw. \texttt{SCORE\_UPWARDS}) oder dass es wichtiger sei, schnell\footnote{Mit ,,schnell`` ist hier ,,mit möglichst wenig Zwischenpunkten`` gemeint.} zum Ziel zu kommen, anstatt eine gute Strecke zu erhalten (\texttt{SCORE\_X} bzw. \texttt{SCORE\_DISTANCE}).

Bei drastisch ausweglosen Beispielen (so etwa in Labyrinthen, in denen die hier vorgestellten Algorithmen vermutlich deutlich langsamer würden) kann auch das interne Rekursionslimit \texttt{PATHFINDER\_LIMIT} erhöht werden. Dabei dient \texttt{PATHFINDER\_LIMIT} allerdings \emph{nicht} als Beschränkung der Tiefe der Suche -- so macht es ja Python bereits bei 1000 Schritten \footnote{Dieses Limit kann mit \texttt{sys.getrecursionlimit()} betrachtet und mit \texttt{sys.setrecursionlimit()} bei Bedarf erhöht werden.}.

Stattdessen ist damit ein Limit für die schlechten Schritte bei \texttt{Processor.nextStep} gemeint. Standardmäßig wird immer versucht, nicht nach ,,hinten`` zu gehen, da dies natürlich zu einer schlechteren Route führen würde. Sollte es keine andere Möglichkeit geben, so muss natürlich ein Schritt nach hinten gemacht werden -- ganz klar. Wie viele maximal nach hinten gehen dürfen wird durch dieses Limit angegeben.

\subsubsection{Generierung der Vektorgrafiken}

Die Klasse \texttt{ImageGenerator} des Moduls \texttt{generator} kümmert sich um die korrekte Erstellung der Vektorgrafiken, die auch in dieser Dokumentation verwendet werden. Grundstein sind dabei drei vorgefertigte Vorlagen, die mit den korrekten Daten nur noch gefüllt und zusammengesetzt werden müssen. Diese Vorlagen können bei Bedarf auch angepasst werden, soweit es der Wunsch eines Nutzers sein sollte.

Der Bildgenerator muss mit einer Referenz auf eine Umgebung (\texttt{Environment}) initialisiert werden. Die Methode \texttt{write} kümmert sich dann um das Generieren und Speichern der Bilddaten. Hauptbestandteil dieses Generierens ist die Methode \texttt{generate}, die einen String zurück liefert, der als gültiger Code in eine SVG-Datei geschrieben werden kann. Dabei erwartet die Methode lediglich eine Liste von Listen mit Tuples, die Koordinaten widerspiegeln. Darauf basierend sollen zusätzliche Linien (Unabhängig vom Pfad) in die Datei eingefügt werden können.

Besonders durch den Einsatz von List Comprehensions konnte der Code in \texttt{generate} sehr stark komprimiert werden.
