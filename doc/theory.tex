\section{Lösungsidee}

Im Grunde handelt es sich beim vorgestellten Problem von Lisa um ein Problem der Wegsuche. Für optimale Ergebnisse wurde dieses Problem in zwei Teilprobleme unterteilt: die Erstellung eines Graphen und die Wegsuche in diesem. Beide Teilprobleme werden im Folgenden behandelt.

\subsection{Grundlagen}

\subsubsection{Kürzeste Verbindung zwischen zwei Punkten}
\label{subsubsec:shortest-path-general}

Wie allgemein bekannt ist, ist die kürzeste Verbindung zweier Punkte in der euklidischen Fläche die Strecke zwischen diesen beiden Punkten. In den im Folgenden beschriebenen Algorithmen ist der kürzeste Weg zwischen einem Startpunkt und einem Ziel gesucht. Dieser besteht demnach aus möglichst wenigen Kurven -- nämlich gar keinen --, sondern nur Geraden bzw. Strecken.

Falls also ein Weg den Punkten $A$, $B$ und $C$ folgt, aber eine gerade Linie zwischen $A$ und $C$ ebenso begangen werden kann, so wird dies getan, um den Umweg über $B$ zu vermeiden. Falls $B$ auf $[AC]$ läge, wäre es rechnerisch kein Unterschied, aber vereinfacht die Handhabung, da die Anzahl der Punkte reduziert wird.

Daraus ergibt sich des Weiteren, dass alle Punkte des Pfads, die nicht Start- oder Endpunkt sind, Ecken eines Polygons sein müssen. Denn knapp um ein Polygon herum zu laufen, aber beispielsweise einen Abstand von zwei Metern zu halten, macht keinen Sinn, schließlich kann direkt auf den Kanten des Polygons gelaufen werden. Diese Information ist dahingehend sehr nützlich, als dass es sich damit um ein diskretes Problem handelt und der in Abschnitt \ref{subsec:visibility-graph} vorgestellte Graph damit ebenfalls diskret ist\footnote{Ausgenommen natürlich der Start- und Endpunkt des Pfades.}.

\subsubsection{Beschränkungen}

Es wird angenommen, dass ausschließlich Punkte in der 2D-Fläche und nur im ersten Quadranten auftauchen. Des Weiteren wird angenommen, dass stets ein Weg von Lisa zur Busstrecke existiert\footnote{Sollte dem nicht so sein, behandelt das Programm den Fall allerdings korrekt.}. Ein Polygon kann im oder gegen den Uhrzeigersinn definiert sein, aber es treten in keinem Polygon zwei Punkte zweimal auf -- Polygone treffen sich also nicht in gemeinsamen Punkten. Zudem wird angenommen, dass die Polygone sich nicht überlappen.

\subsubsection{Definitionen}

\paragraph{Freier Knoten}
Als einen \emph{freien Knoten} betrachtet man einen Knoten, der eine nicht blockierte Sichtlinie entlang der Optimallinie zur y-Achse (Straße) hat. Auf die Optimallinie wird in Abschnitt \ref{subsubsec:best-path-specific} näher eingegangen.

\paragraph{Sichtlinie}
Eine \emph{Sichtlinie} ist eine Gerade (hier jedoch stets eine Strecke), die von einem Punkt zu einem anderen Punkt verläuft. Es handelt sich um keine Verbindungsstrecke zwischen zwei Nachbarpunkten eines Polygons, sondern um die Strecke zwischen beliebigen Punkten. Sie ist \emph{blockiert}, sofern mindestens ein Schnittpunkt mit mindestens einer anderen Kante vorhanden ist und dieser Schnittpunkt nicht in einem Eckpunkt der Kanten liegt. Andernfalls -- wenn dies also für keine einzige Kante gilt -- ist die Sichtlinie \emph{frei}.

\subsection{Verworfene Ansätze}

\subsubsection{Folgen der Ideallinie}

Ein Ansatz zur Lösung des Gesamtproblems war, die Optimallinie von Lisa zur Busstrecke zu ziehen. Diese Linie würde in den meisten Fällen irgendwelche Hindernisse kreuzen. Es würde geprüft, welches Hindernis die Linie schneidet und dieses dann auf den Kanten des Polygons entsprechend umgangen. Nach jedem Schritt dieses iterativen Verfahrens würde die Route optimiert, um Fälle, wie in Abschnitt \ref{subsubsec:shortest-path-general} beispielhaft vorgestellt, zu vermeiden. Als Ziel bzw. Orientierung dient dabei stets die Ideallinie, sodass bei Nutzung des besten Wegs um die beiden möglichen Seiten eines Polygons herum auch wieder ein optimaler Weg entstehen sollte.

\subsubsection{Rapidly-Exploring Random Trees}

Das Konzept der \emph{Rapidly-Exploring Random Trees}\cite{rrt-basic} (RRT) bzw. die verbesserte Variante mit \emph{RRT*-Smart}\cite{rrt*-smart} klang bei der Diskussion der unterschiedlichen Lösungsansätze neben dem Visibility Graph am vielversprechendsten. Der \emph{informierte} Ansatz hat eine asymptotische Annäherung ans Optimum und sollte in $O(n)$ für Anfragen und $O(n \log n)$ für die Verarbeitung ein sehr schneller Algorithmus sein. Dennoch wurde letzten Endes die Variante des Visibility Graph verwendet, da die dafür nötigen Datenstrukturen und Algorithmen bereits fast vollständig implementiert waren.

\subsection{Visibility Graph}
\label{subsec:visibility-graph}

Aus allen eingegebenen Hindernissen (\emph{polygons} bzw. \emph{obstacles}) wird ein ungewichteter\footnote{Es existiert eine boolesche Gewichtung, dazu später mehr.}, ungerichteter, nicht vollständiger Graph produziert. Dieser \emph{Visibility Graph} (VG) besteht aus allen Knoten (\emph{vertices}), die Elemente der Polygone $P$ sind, sodass $V \in P$ für jedes $V$ gilt.

Eine Kante (\emph{edge}) $E$ wird nun zwischen einem Startknoten $V_S$ und einem Endknoten $V_E$ (oder vice-versa, da ungerichtet) erstellt, sodass die folgende Bedingung erfüllt ist:

\begin{center}
	\ovalbox{Kein Punkt $p \in E$ mit $p \not= V_S$ und $p \not= V_E$ liegt in einem anderen Hindernis}
\end{center}

Als \emph{anderes} Hindernis ist dabei stets ein solches Hindernis $P'$ gemeint, dass $V_S \not\in P'$ und $V_E \not\in P'$ gilt. Denn schließlich ist es vollkommen legitim, dass ein Knoten $V$ Teil einer Kante eines Polygons ist.

\subsubsection{Sichtbarkeit}

Aus dieser Definition leitet sich die \emph{Sichtbarkeit} ab. Ein Knoten $V_E$ ist von einem Knoten $V_S$ aus sichtbar, falls die Verbindungsstrecke $(V_S V_E)$ kein Polygon schneidet. Eine Kante, die die Bedingung der Sichtbarkeit erfüllt, ist für die spätere Pfadsuche nützlich, da sie ,,begehbar`` ist -- Lisa kann genau diesen Weg entlang laufen.

Ob eine Kante $(V_S V_E)$ sichtbar ist, kann geprüft werden, indem für jeden Knoten des Graphen $v$ (mit $v \not= V_S$ und $v \not= V_E$) der Schnittpunkt zwischen dessen Kanten und der Kante $(V_S V_E)$ bestimmt wird. Gibt es gar keinen Schnittpunkt mit gar keinem Punkt $v$, so gilt $(V_S V_E)$ als ,,begehbar``.

Damit ergibt sich quasi eine Gewichtung für eine Kante. Entweder sie ist ,,begehbar``, dann hätte sie die Gewichtung $0$. Oder sie ist es nicht, da ein Hindernis den Weg zwischen $V_S$ und $V_E$ versperrt. In letzterem Fall wäre die Gewichtung $\infty$. Da eine Kante, die das ,,Sichtbarkeitskriterium`` verletzt, zum Graphen aber gar nicht erst hinzugefügt wird, ist es nicht weiter relevant: der Graph besteht ausschließlich aus ,,begehbaren`` Kanten.

Aus Gründen der verbesserten Performance und einfacheren Implementierung werden im Graphen nicht die begehbaren, sondern die blockierten Kanten gespeichert. Sobald der Graph vollständig konstruiert wurde, werden Anfragen der Form ,,ist diese Kante blockiert?\grqq{} gestellt (im Gegensatz zu ,,ist diese Kante begehbar?\grqq{}).

Für den Sonderfall, dass eine Kante von $v$ auf $(V_S V_E)$ liegt, gibt es unendlich viele Schnittpunkte. Da Lisa die Kanten aber entlang laufen kann, gilt $(V_S V_E)$ dann immer noch als ,,begehbar``.

\subsubsection{Schnittpunktberechnung}
\label{subsubsec:intersection-calculation}

Eine Gerade verlaufe durch die Punkte $P_1 = \begin{pmatrix} x_1 \\ y_1 \end{pmatrix}$ und $P_2 = \begin{pmatrix} x_2 \\ y_2 \end{pmatrix}$ und eine zweite Gerade verlaufe durch $P_3 = \begin{pmatrix} x_3 \\ y_3 \end{pmatrix}$ und $P_4 = \begin{pmatrix} x_4 \\ y_4 \end{pmatrix}$.

Den Schnittpunkt $P_S = \begin{pmatrix} x_S \\ y_S \end{pmatrix}$\footnote{Siehe dazu auch Abschnitt \ref{subsubsec:calculation-intersection}.} berechnet man dann wie folgt\cite{wiki:intersection}:

$$x_S = \frac{(x_4-x_3)(x_2 y_1-x_1 y_2) - (x_2-x_1)(x_4y_3-x_3y_4)}{(y_4-y_3)(x_2-x_1) - (y_2-y_1)(x_4-x_3)}$$

$$y_S = \frac{(y_1-y_2)(x_4 y_3 - x_3 y_4) - (y_3-y_4)(x_2 y_1-x_1 y_2)}{(y_4-y_3)(x_2-x_1) - (y_2-y_1)(x_4-x_3)}$$

\subsubsection{Berechnung}

Über die Berechnung des Visibility Graphs haben sich bereits viele Leute Gedanken gemacht. Dabei gibt es viele verschiedene Ansätze\cite{kitzinger}, die in ihrer Effizienz teils sehr große Unterschiede aufweisen. Der naive Ansatz arbeitet mit einer asymptotischen Laufzeit von $O(n^3)$. Eine verbesserte Version von D. T. Lee\cite{lee}\cite{visibility-graph} verwendet eine \emph{Scan Line} und kommt damit auf eine Laufzeit von $O(n^2 \log n)$.

Es wurden in den folgenden Jahren viele weitere Ansätze erarbeitet. Die Laufzeit wurde dabei bis auf $O(n^2)$ bei linearem Speicherbedarf reduziert. Mit einem komplexeren Ansatz von Ghosh und Mount kann sogar $O(|E| + n \log n)$ erreicht werden, was bei wenigen Kanten ein sehr günstiger Algorithmus ist. Bei einem nahezu vollständigen Graph mit $|E| \approx n^2$ ist dieser Algorithmus aber nicht günstig.

In diesem Projekt wird im Grunde auf keine standardisierte Variante gesetzt, sondern ein mit vielen kleinen Verbesserungen versehener naiver Ansatz. Mehr dazu im Abschnitt \ref{subsubsec:visgraph-running-time}.

\subsubsection{Speicherung der Kanten}

Zur Speicherung der Kanten wird eine negierte \emph{Adjazenzmatrix} verwendet. Dafür können speicher-technisch vorteilhaft assoziative Arrays und als Werte dafür Sets von Knoten benutzt werden. Sets (Mengen) sind günstiger als gewöhnliche Listen, da dann keine Knoten mehrfach gespeichert werden können. Und eine Reihenfolge der Elemente ist nicht notwendig.

\subsection{Pfadsuche}

Da nun ein Graph mit $n$ Knoten und zwischen $n$ und $n^2$ Kanten gebildet worden ist, kann die Pfadsuche vom Startknoten bis zur y-Achse des Koordinatensystems (der Straße) beginnen. Da es sich um ein Problem der Klasse \emph{Single-Source Shortest Path} handelt, kann dazu jeder bekannte Algorithmus zur Lösung dieses Problems angewendet werden. Beispiele hierfür sind die iterative Breitensuche, der Dijkstra- oder der A*-Algorithmus.

\subsubsection{Beste Wege}
\label{subsubsec:best-path-specific}

Wie in Abschnitt \ref{subsubsec:shortest-path-general} bereits erläutert worden ist, sind Strecken die besten Verbindungen zwischen zwei Punkten. Aber wie sieht es bei Lisa und dem Bus aus? Was ist die beste Verbindungsstrecke zwischen einer Position $L$ und der y-Achse?

Die nachfolgenden Gleichungen verwenden unter $L_x$ die x-Koordinate bzw. unter $L_y$ die y-Koordinate des Punktes von Lisa, der betrachtet werden soll.

Der allgemeine Ansatz ist, die Zeit, die Lisa vor dem Bus los laufen muss, zu bestimmen:

\begin{equation}
	f(y) = t_L - t_B
\end{equation}

\begin{equation}
	f(y) = \frac{s_L}{v_L} - \frac{y}{v_B}
\end{equation}

Die Strecke $s_L$ von Lisa bis zum Treffpunkt ist nicht bekannt, schließlich soll keine Parallele zur x-Achse verwendet werden, sondern die optimale Route in einem bestimmten Winkel. Unter Ausnutzung des Satz von Pythagoras lässt sich $s_L$ so darstellen:

\begin{equation}
	f(y) = \frac{\sqrt{L_x^2 + (L_y - y)^2}}{v_L} - \frac{y}{v_B}
\end{equation}

Damit ist auch die vollständige Funktion aufgestellt, die in Abhängigkeit vom Schnittpunkt mit der y-Achse das Zeitdelta berechnet.

Diese Funktion wird nun abgeleitet und ergibt:

\begin{equation}
	f'(y) = \frac{L_y - y}{v_L\sqrt{L_x^2 + (L_y - y)^2}} - \frac{1}{v_B}
\end{equation}

Um die Extremstelle zu berechnen, muss die Ableitungsfunktion gleich $0$ gesetzt werden:

\begin{equation}
	f'(y) = 0
\end{equation}

Löst man die Ableitung $f'$ nun nach $y$ auf, so erhält man einen Ausdruck, der den y-Achsenabschnitt in Abhängigkeit von den beiden (konstanten) Geschwindigkeiten und der Position von Lisa bestimmt (dies ist das Minimum der Funktion):

\begin{equation}
	y = \sqrt{- \frac{v_L^2 L_y^2}{v_L^2 - v_B^2}}+L_y
\end{equation}

Dass die hier vorgestellte Methode tatsächlich richtig ist, wurde experimentell in Abschnitt \ref{subsubsec:experimental-best-routes} bestätigt. Übrigens verlaufen die optimalen Wege im Winkel von 30 Grad zur x-Achse, was mit dem Verhältnis der Geschwindigkeiten korreliert\footnote{Dies ist eine aus der Formel abgeleitete Beobachtung und keine zu beweisende These. Im Übrigen gilt diese Behauptung nur solange $v_L < v_B$ gilt.}:

\begin{equation}
	\sin(\alpha) = \frac{v_L}{v_B}
\end{equation}

\subsubsection{Experimentieller Beweis für beste Routen ohne Hindernisse}
\label{subsubsec:experimental-best-routes}

Angenommen, Lisa startete im Punkt $(100,100)$, der Bus wie gewöhnlich im Ursprung bei $(0,0)$ und es gäbe \textbf{keine} Hindernisse auf der Karte. Wie müsste Lisa optimal gehen?

\paragraph{Naiver Ansatz}
Nach dem Prinzip des kürzesten Wegs nimmt Lisa die Parallele zur x-Achse und läuft geradewegs zum Punkt $(0,100)$. Sie legt demnach $100$m in $24$s zurück, der Bus braucht lediglich die Hälfte der Zeit, sodass Lisa schlussendlich $12$s vor Abfahrt des Busses los laufen müsste.

\paragraph{Optimaler Ansatz}
Idealerweise läuft Lisa aber zu dem Punkt, der ihr durch die in Abschnitt \ref{subsubsec:best-path-specific} hergeleitete Formel angegeben wird. Im Fall für den Ausgangspunkt $(100,100)$ ergibt sich der Idealpunkt bei $y \approx 157.735$ und damit dem Treffpunkt von $(0,158)$\footnote{Die Punkte des Graphen werden auf ganze Zahlen genau gespeichert. Es wäre zwar möglich, Fließkommazahlen zu verwenden, doch die Aufgabenstellung gibt nur Ganzzahlen vor.}. Sie bräuchte für die Strecke von $115.6$m rund $27.7$s, der Bus bräuchte für $158$m rund $18.9$s. Sie müsste daher $8.8$s vor dem Bus los laufen, um den Treffpunkt in genau diesem Augenblick zu erreichen.

\subsubsection{Bewertung von Knoten}
\label{subsubsec:theory-scoring}

Es gibt eine Bewertungsfunktion für Knoten, basierend auf dem aktuellen Knoten. Je kleiner die Bewertung (der Score), die von dieser Funktion zurück gegeben wurde, desto eher wird dieser Knoten verglichen mit einem anderen als nächster verwendet. Die Bewertung stützt sich auf drei Aspekte:

\begin{itemize}
	\item Abstand des Knoten von der y-Achse
	\item Distanz des letzten Knoten zum bewerteten Knoten
	\item Unterscheidung der Steigung der Verbindungsstrecke mit einem Faktor
\end{itemize}

Insbesondere der letzte Punkt bedarf einer Erklärung: die Steigung wird als Maß dafür verwendet, ob ein Knoten nach ,,oben`` oder ,,unten`` geht. Die Idee dahinter ist, Knoten, die weiter im Norden liegen, zu bevorzugen, sodass Lisa u. U. noch Zeit gewinnt (siehe Abschnitt \ref{subsubsec:best-path-specific}).

Jeder der hier diskutierten Punkte erhält überdies einen konstanten Vorfaktor, sodass bei Variation der Vorfaktoren betrachtet werden kann, wie sich die Bewertungsfunktion verhält.

\subsubsection{Rekursiv schrittweises Vorgehen}

Obwohl jeder Routen-Algorithmus verwendet werden könnte, wird hier auf eine einfache Form der \emph{rekursiven, zielgerichteten Tiefensuche} zurückgegriffen. Für einen Startknoten $V_S$ werden folgende Schritte unternommen, um die Liste von \emph{targets} zu bestimmen:

\begin{enumerate}
	\item Gebe den Pfad $[V_E, V_S]$ zurück, sofern $V_S$ ein freier Knoten ist
	\item Erstelle eine leere Liste $T$ für die \emph{targets}
	\item Füge jeden Knoten $V$ des Graphen zu $T$ hinzu, sofern er ein bewiesen sichtbarer Knoten von $V_S$ ist
	\item Sortiere $T$ gemäß der Bewertungsfunktion
\end{enumerate}

Die nun aufsteigend sortierte Liste $T$ enthält alle Knoten, die von $V_S$ direkt sichtbar und erreichbar sind. Diese Liste wird nun abgearbeitet, indem diese rekursive Funktion für jeden Knoten $V_{next}$ dieser Liste durchgeführt wird. Da das Ergebnis der Funktion entweder eine Liste mit Knoten (= Pfad gefunden) oder eine leere Liste (= kein Pfad gefunden) ist, kann auch geprüft werden, ob die Berechnung dieses Knotens $V_{next}$ erfolgreich war.

Der erste erfolgreiche Pfad wird um den Startknoten $V_S$ erweitert und weiter an die nächsthöhere Ebene zurück gegeben. Das Anhängen von $V_S$ ist das Wiederfinden des Pfads: Backtracking.

\subsection{Optimierungen}

\subsubsection{Zugriff auf die Knoten des Graphen}

In mehreren Schritten des Programms, sowohl beim Visibility Graph als auch bei der Pfadsuche, müssen alle Knoten des Graphen durchlaufen werden. Da allerdings nicht notwendigerweise alle Knoten des Graphen auch tatsächlich relevant sind, wird diese Liste aller Knoten stets sortiert zurück gegeben.

Die Sortierung wird von den Knoten selbst übernommen. Je kleiner die x-Koordinate eines Knoten ist, desto früher soll seine Position in der Liste des Graphen sein. Bei gleichen x-Koordinaten ist eine kleinere y-Koordinate der ,,bessere`` Wert, sodass bei zwei Knoten mit gleichem x-Wert der kleinere y-Wert früher kommt.

Welcher Zweck mit dieser Sortierung verfolgt? Bei einer Iteration über diese Liste werden die Knoten nun (graphisch gesehen) von links nach rechts abgearbeitet. Da z. B. bei der Pfadsuche der Pfad nach ,,ganz links`` gesucht ist, kann die Iteration abgebrochen werden, sobald der Startknoten erreicht worden ist: nachfolgende Knoten werden nicht mehr als mögliche Ziele in Betracht gezogen.

Gleiches gilt für die Generierung des Visibility Graph und erklärt, warum die Prüfung der Sichtbarkeit eines Knoten (bei der auch diese Liste zum Einsatz kommt), länger dauert, je weiter rechts der Knoten liegt: der $i$-te Knoten muss mit allen Vorgängern und damit $i-1$ Knoten verglichen werden.

Dieses Vorgehen ist eng mit der sogenannten \emph{Sweep Line} verwandt.

\subsubsection{Weitere Verbesserungen}

Viele Verbesserungen sind bereits in anderen Teilen dieses Dokuments erklärt worden und werden daher hier nicht mehr aufgeführt. Dazu gehört beispielsweise die sehr verkleinerte Adjazenzmatrix mit nur $2$ Elementen pro Eintrag und damit einem Speicherbedarf von $3n$.

\subsection{Optimalität}
\label{subsec:theory-optimum}

Die generierten Pfade setzen immer auf das nächste lokale Optimum\footnote{Gemäß der Gewichtung des einzelnen Knoten -- eine große Rolle spielen daher auch die Scoring-Vorfaktoren.}. Dabei \emph{kann} aber der Fall eintreten, dass das globale Optimum verfehlt wird. In den Abschnitten \ref{tab:results-overview} bis \ref{subsubsec:results-discussion} werden die Beispieleingaben diskutiert und näher darauf eingegangen, ob die Ergebnisse tatsächlich optimal sind.

Dabei wurden die Standardwerte für die Bewertungsfunktion verwendet, wie sie in der Tabelle zu Beginn von Abschnitt \ref{tab:default-scores} dargestellt sind. Änderungen dieser Werte können zu besseren wie auch schlechteren Ergebnissen führen.

\subsection{Theoretische Laufzeitanalyse}

\subsubsection{Visibility Graph}
\label{subsubsec:visgraph-running-time}

Allgemein wird jede Kante mit jeder Kante, und damit jeder Knoten mit jedem Knoten und jeder Knoten mit jedem Knoten, verglichen. Damit ergibt sich eine theoretische Laufzeit von $O(n^4)$ für $n$ Knoten. Durch die Optimierungen wird diese theoretische Zeit reduziert:

\begin{enumerate}
	\item Da nur Kanten, die auch existieren, geprüft werden müssen, reduziert sich die Anzahl der Prüfungen um den Faktor $\frac{n}{2}$, da für gewöhnlich jeder Knoten genau zwei Nachbarn hat
	\item Durch die Verwendung einer Sweep Line müssen für einen Knoten an der $i$-ten Position lediglich $i-1$ andere Knoten getestet werden (damit dauert das Verfahren pro Knoten länger, je später der Knoten kommt)
	\item Durch das Abbrechen der ,,Sichtbarkeitsprüfung``, sobald sie gescheitert ist, reduziert sich die Laufzeit diese Operation ungefähr auf die Hälfte im Vergleich zu vorher
\end{enumerate}

Insgesamt sollte die theoretische Laufzeit $O(n^3)$ sein, auch wenn die effektive Laufzeit deutlich darunter liegt. Denn die oben vorgestellten und auch sonst erläuterten Verbesserungen sind nicht eindeutig in Landau-Notation ausdrückbar. In der Praxis geht die Laufzeit für $n$ Knoten durchschnittlich gegen $O(n^2)$, auch wenn die tatsächliche Laufzeit nicht nur von der Zahl der Knoten, sondern auch der Anzahl der Hindernisse und deren Konstellation abhängt.

\subsubsection{Pfadsuche}

Im schlimmsten Fall hat die Pfadsuche -- wie eine gewöhnliche iterative Tiefensuche -- die Laufzeit $O(|E| + |V|)$ bei $E$ Kanten und $V$ Knoten im Graphen. Dadurch, dass allerdings bereits ein Visibility Graph mit bedeutend weniger Kanten erstellt worden ist und die zu testenden Knoten gemäß einer Bewertung sortiert werden, verbessert sich diese obere Grenze in der Praxis stark nach unten. Eine asymptotische Laufzeitangabe in Landau-Notation ist allerdings nicht möglich.

\subsubsection{Gesamtheit}

Insgesamt überwiegt in diesem Projekt die Laufzeit, die für die Berechnung des Visibility Graph benötigt wird. Für die theoretische Laufzeit ist daher ausschließlich die Berechnung des Visibility Graph ausschlaggebend. In der Praxis benötigt der Visibility Graph -- je nach Eingabe -- zwischen 84\% der Laufzeit für das einfachste Beispiel und bis zu 98\% für die größeren Dateien.

Unter der Annahme, dass die Annahme von $O(n^3)$ für den Visibility Graph theoretisch richtig ist, ergibt sich demnach eine theoretische Gesamtlaufzeit von $O(n^3 + |E| + |V|) = O(n^3)$.
