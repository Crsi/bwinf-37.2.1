#!/usr/bin/env python3

"""
"""

import functools
from typing import Sequence, Union


@functools.total_ordering
class Vertex:
	"""
	Vertex that will be used as a point in the coordinate system
	
	To work correctly, the Vertices' method "setup" has to be called
	once after the initialization of all nodes in the environment.
	"""
	
	__slots__ = ["x", "y", "free", "id"]
	
	def __init__(self, x: Union[Sequence[int], int], y: int = None):
		"""
		Constructs the Vertex object
		
		It's possible to initialize the Vertex in two ways.
		In the one form it's a sequence of exactly two elements,
		while it's simply the two coordinates in the other form.
		
		Note that a set is no Sequence! Usually, using a dict does not
		really work because the method wants "something" with indexes.
		
			>>> import vertices as v
			>>> v.Vertex(2, 4)
			Vertex(2, 4)
			>>> v.Vertex(-5, 0)
			Vertex(-5, 0)
			>>> v.Vertex([1, -2])
			Vertex(1, -2)
			>>> v.Vertex({0: 8, 1: 4})
			Vertex(8, 4)
			>>> v.Vertex([0, 1, 2])
			Traceback (most recent call last):
				[...]
			ValueError: Expected len(obj) == 2
		
		:param x: sequence of two elements or the x-coordinate
		:param y; y-coordinate or None
		"""
		
		# Decide which form of the constructor has been used
		if y is None and hasattr(x, "__iter__"):
			
			# In this form, x is an iterable object so it should
			# have exactly two elements that are the coordinates
			if len(x) != 2:
				raise ValueError("Expected len(obj) == 2")
			
			# Coordinate values of the point
			self.x = int(x[0])
			self.y = int(x[1])
		
		elif isinstance(x, int) and isinstance(y, int):
			
			# In this form, x and y are given exactly as their values
			self.x = x
			self.y = y
		
		else:
			raise ValueError("Wrong use of the Vertex constructor!")
		
		# Flag whether the node could have a straight line to the y-axis
		self.free = False
		
		# Reference to the Polygon ID used to identify it in the graph
		self.id = -1
	
	def __getitem__(self, index: int) -> int:
		"""
		Returns the coordinate of the Vertex specified by the index
		
		:param index: integer indicating the x (0) or y (1) coordinate
		:return: x/y coordinate
		"""
		
		if index == 0:
			return self.x
		elif index == 1:
			return self.y
		else:
			raise IndexError("Index out of range")
	
	def __hash__(self):
		"""
		Returns actually object.__hash__(self)
		
		:return: hash(self)
		"""
		
		return object.__hash__(self)
	
	def __repr__(self) -> str:
		"""
		Returns repr(self)
		
		:return: valid string representation of object Node
		"""
		
		return "Vertex({}, {})".format(self.x, self.y)
	
	def __lt__(self, obj) -> bool:
		"""
		Returns whether obj has the higher x value (y value if equal)
		
		:param obj: Vertex object
		:return: self < object
		"""
		
		if not isinstance(obj, Vertex):
			raise TypeError("Can't compare Vertex with " + str(type(obj)))
		return self.y < obj.y if (self.x == obj.x) else self.x < obj.x
	
	def __eq__(self, obj) -> bool:
		"""
		Returns whether both Vertices are on the same point
		
		:param obj: Vertex object
		:return: self == object
		"""
		
		if not isinstance(obj, Vertex):
			raise TypeError("Can't compare Vertex with " + str(type(obj)))
		return self.x == obj.x and self.y == obj.y
