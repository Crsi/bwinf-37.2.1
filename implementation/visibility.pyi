#!/usr/bin/env python3

from typing import Optional, Tuple, Union

from vertices import Vertex
from edge import Edge
from graph import Graph


def slope(A: Vertex, B: Vertex) -> Optional[float]:
	...


def getYIntersection(pos: Union[Tuple[int, int], Vertex]) -> float:
	...


def find(e1: Edge, e2: Edge) -> Optional[Tuple[float, float]]:
	...


def findIntersection(a: Vertex, b: Vertex, c: Vertex, d: Vertex) -> Optional[Tuple[float, float]]:
	...


def isVisible(start: Vertex, destination: Vertex, graph: Graph) -> bool:
	...
