#!/usr/bin/env python3

"""
"""

import math
from typing import Optional, Tuple, Union

from vertices import Vertex
from edge import Edge
from graph import Graph

from constants import *


def slope(A: Vertex, B: Vertex) -> Optional[float]:
	"""
	Returns the slope of line from A to B
	
	:param A: 2D point in the coordinate system as Vertex
	:param B: 2D point in the coordinate system as Vertex
	:return: slope of [AB] or None if parallel to y-axis
	"""
	
	dx = A.x - B.x
	if dx == 0:
		return None
	return (A.y - B.y) / dx


def getYIntersection(pos: Union[Tuple[int, int], Vertex]) -> float:
	"""
	Returns the best intersection with the y-axis

	:param pos: current position of Lisa as Vertex or tuple of integers
	:return: value of the y-intersection using the optimal route
	"""
	
	return pos[1] + math.sqrt(
		- SPEED_LISA ** 2 * pos[0] ** 2 / (SPEED_LISA ** 2 - SPEED_BUS ** 2)
	)


def find(e1: Edge, e2: Edge) -> Optional[Tuple[float, float]]:
	"""
	Returns a tuple containing the intersection of both edges or None
	
	:param e1: Edge 1
	:param e2: Edge 2
	:return: intersection point as tuple (!) or None
	"""
	
	# Calculate the intersection point using the generic formula
	try:
		point = (
			(
				(e2.b.x - e2.a.x) * (
					e1.b.x * e1.a.y - e1.a.x * e1.b.y
				) - (e1.b.x - e1.a.x) * (
					e2.b.x * e2.a.y - e2.a.x * e2.b.y
				)
			) / (
				(e2.b.y - e2.a.y) * (e1.b.x - e1.a.x)
				- (e1.b.y - e1.a.y) * (e2.b.x - e2.a.x)
			),
			(
				(e1.a.y - e1.b.y) * (
					e2.b.x * e2.a.y - e2.a.x * e2.b.y
				) - (e2.a.y - e2.b.y) * (
					e1.b.x * e1.a.y - e1.a.x * e1.b.y
				)
			) / (
				(e2.b.y - e2.a.y) * (e1.b.x - e1.a.x)
				- (e1.b.y - e1.a.y) * (e2.b.x - e2.a.x)
			)
		)

	# In case there's a ZeroDivisionError, the lines don't intersect
	except ZeroDivisionError:
		return None
	
	# Verify that the point is part of both line segments (Edges)
	if not e1.hits(point):
		return None
	if not e2.hits(point):
		return None
	
	return point


def findIntersection(
	a: Vertex, b: Vertex, c: Vertex, d: Vertex
) -> Optional[Tuple[float, float]]:
	"""
	Returns a tuple containing the intersection of both edges or None
	
	Note that this is a wrapper function around 'find'. The intersection
	is searched for the line segments [AB] and [CD].
	
	:param a: 2D point in the coordinate system as Vertex
	:param b: 2D point in the coordinate system as Vertex
	:param c: 2D point in the coordinate system as Vertex
	:param d: 2D point in the coordinate system as Vertex
	:return: intersection point as tuple (!) or None
	"""
	
	return find(Edge(a, b), Edge(c, d))


def isVisible(start: Vertex, destination: Vertex, graph: Graph) -> bool:
	"""
	Checks if destination is visible from start in the given Graph
	
	:param start: 2D point in the coordinate system as Vertex
	:param destination: 2D point in the coordinate system as Vertex
	:param graph: visibility graph that's used here
	:return: whether the line from start to destination is not blocked
	"""
	
	# Checking against all other vertices and their edges is needed
	for a in graph.get(Vertex):
		for b in graph.adjacency[a]:
			
			# Don't check the same vertices of course
			if start in (a, b) or destination in (a, b):
				continue
			
			# Compute the "new" visibility
			if findIntersection(start, destination, a, b):
				return False
			
	return True
