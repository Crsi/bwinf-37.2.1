#!/usr/bin/env python3

DEBUGGING: bool
LOGGING: bool


def debug(*args, **kwargs) -> None:
	...


def info(*args, **kwargs) -> None:
	...


def error(*args, **kwargs) -> None:
	...
