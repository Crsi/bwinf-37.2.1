#!/usr/bin/env python3

"""
"""

import argparse
from typing import List, Optional

import log
import visibility
from edge import Edge
from graph import Graph
from vertices import Vertex
from environment import Environment
from generator import ImageGenerator

from constants import *


class Processor:
	"""
	Processor of Environments that creates and solves the visibility graph
	"""

	@classmethod
	def calc(cls, path: List[Vertex]) -> float:
		"""
		Returns the absolute length of the path
		
		:param path: list of vertices
		:return: length of the path (including both end points)
		"""
		
		length = 0
		for i in range(len(path) - 1):
			length += Edge(path[i], path[i + 1]).distance
		return length
	
	def __init__(self, environment: Environment, args: argparse.Namespace):
		"""
		Constructs a Processor object
		
		:param environment: valid per-built Environment
		:param args: Namespace object as returned by main routine
		"""
		
		# Environment that is used to find a way to the y-axis
		self.environment = environment
		
		# Namespace object to further configure the Processor
		self.args = args
		
		# Generator for SVG outputs that's used during the processing
		self.generator = ImageGenerator(environment, "UTF-8")
		
		# Number of steps the program has already done
		self.step = 0
	
	def nextStep(
		self, start: Vertex, graph: Graph, escape: int = 0,
		destination: Optional[Vertex] = None
	) -> List[Vertex]:
		"""
		Calculates the next step of the recursive path finder
		
		:param start: (current) start Vertex
		:param graph: graph that may also be a full visibility graph
		:param escape: how often the optimization should be turned off
		:param destination: target Vertex (or None if y-axis)
		:return: list of vertices towards the target
		"""
		
		def f(v: Vertex):
			"""
			Returns the score of a Vertex

			Note that this function also uses variables from the
			outer scope! Some vertices may or may not have the same score.

			:param v: Vertex object that should be scored
			:return: score of the specific Vertex
			"""
			
			# The slope of the distance line is also useful for scoring!
			m = visibility.slope(v, start)
			
			# If the slope is "None", both vertices have the same x-coordinate
			if m is None:
				if v.y > start.y:
					smart = SCORE_UPWARDS
				else:
					smart = SCORE_DOWNWARDS
			
			# Otherwise, a Vertex that goes "up" in any way is preferred
			else:
				if (m > 0 and v.x > start.x) or (m < 0 and v.x < start.x):
					smart = SCORE_UPWARDS
				else:
					smart = SCORE_DOWNWARDS
			
			return smart * (
				SCORE_X * v.x + SCORE_DISTANCE * Edge(v, start).distance
			)
		
		# Break the recursive loop if the target was reached
		if destination is None and start.free:
			return [
				Vertex(
					0,
					int(
						round(visibility.getYIntersection(start), 0)
					)
				),
				start
			]
		if destination is not None and start == destination:
			return [start]
		
		targets = []
		for node in graph.get(Vertex):
			
			# Stop the iteration if the start node is the current node
			# because otherwise we would go back
			if start == node:
				if escape <= 0:
					break
			
			# Maintain a list of targets
			if graph.check((start, node)) and graph.verify(start, node):
				targets.append(node)
		
		# Sort the targets by their key so that the better ones come earlier
		targets.sort(key = f)
		
		# Every target has to be "explored", sorted by their score
		for each in targets:
			path = self.nextStep(each, graph, escape - 1, destination)
			if len(path) > 0 and path[0].x == 0:
				return path + [start]
		
		return []
	
	def optimize(self, path: List[Vertex]) -> List[Vertex]:
		"""
		Optimizes the given path (shortens the distance)

		:param path: current path as list of vertices from start to end
		:return: optimized path from start to destination
		"""
		
		# Iterate over the path and try to find a shorter route
		final = [path[0]]
		for i in range(len(path)):
			if path[i] not in final:
				continue
			
			# Iterate from the "other end" of the path so that it speeds
			# up the procedure if we reach our "start point"
			for e in range(len(path) - 1, 0, -1):
				if e == i:
					break
				
				# Checking the visibility always requires the raw
				# visibility check but also the verification using the graph
				v = visibility.isVisible(
					path[i], path[e], self.environment.graph
				)
				if v and self.environment.graph.verify(path[i], path[e]):
					final.append(path[e])
					break
		
		# In case no better way had been found, the results are equal
		return final
	
	def main(self) -> bool:
		"""
		Executes the main routine of the Processor object
		
		:return: success of the operation (whether a path was found)
		"""
		
		log.info("Building the visibility graph...")
		self.environment.build(self.args.progress)
		
		log.info("Free vertices:", len(self.environment.graph.free()))
		log.debug("Free:", self.environment.graph.free())
		
		log.info("Starting the pathfinder...")
		tries = 1
		result = []
		
		while len(result) == 0:
			log.debug("Starting try", tries)
			if tries > PATHFINDER_LIMIT:
				log.error(
					"Exceeded limit of recursive retries for "
					"the pathfinder. Aborting."
				)
				break
			
			result = self.nextStep(
				self.environment.start, self.environment.graph, tries - 1
			)
			log.debug("Result:", result)
			tries += 1
		
		log.info("Optimizing path...")
		path = self.optimize(result)[::-1]
		log.debug("Old length:", len(result))
		log.debug("New length:", len(path))
		
		log.debug("Fixing environment size...")
		maxX = maxY = 0
		for node in self.environment.graph.get(Vertex):
			maxX = max(maxX, node.x)
			maxY = max(maxY, node.y)
			
		# Note that the last point of the path is not part of the Graph!
		maxX = max(path[-1].x, maxX)
		maxY = max(path[-1].y, maxY)
		
		log.debug("Found maximum values of", maxX, "and", maxY)
		if maxX < DEFAULT_MAP_SIZE[0]:
			maxX = -1
		if maxY < DEFAULT_MAP_SIZE[1]:
			maxY = -1
		self.environment.size = (maxX, maxY)
		
		self.environment.path = path
		return len(result) != 0
