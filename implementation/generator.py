#!/usr/bin/env python3

"""
"""

import os
import re

import log
import polygon
from environment import Environment

from constants import *


class ImageGenerator:
	"""
	Generator for SVG output files
	
	Note that the environment attribute (and also so-called parameters)
	are no simple tuples! That values are "environments" which fully
	describe a test case. So, this tuple consists of the list of polygons
	(obstacles), the position of Lisa's house and a list of all points
	which together create the best path that Lisa could take.
	Have a look at the docstring of the Reader class for an explanation.
	"""
	
	TemplateLine = """
			<polyline
				id="{lineID}"
				points="{coordinates}"
				fill="none"
				stroke="{color}"
				stroke-width="{stroke}"
			/>"""
	
	TemplatePolygon = """
			<polygon
				points="{points}"
				fill="#6B6B6B"
				stroke="#212121"
				stroke-width="2"
			/>"""
	
	TemplateImage = """<svg version="1.1"
	viewBox="0 0 {TotalWidth} {TotalHeight1}">
	<g transform="scale(1 -1)">
		<g transform="translate(0 -{TotalHeight2})">
			<line
				id="y"
				x1="0"
				x2="0"
				y1="0"
				y2="{TotalHeight3}"
				fill="none"
				stroke="#111111"
				stroke-width="4"
			/>
			{polygons}
			<polyline
				id="R"
				points="{path}"
				fill="none"
				stroke="#000080"
				stroke-width="3"
			/>
			{lines}
			<circle
				id="S"
				cx="{lisaX}"
				cy="{lisaY}"
				r="9"
				fill="#80F080"
				stroke="#000080"
				stroke-width="2"
			/>
		</g>
	</g>
</svg>"""
	
	@classmethod
	def transform(cls, array: polygon.Polygon) -> str:
		"""
		Generates a string that can be inserted into the SVG path attribute
		
		:param array: array that contains exactly one polygon
		:return: string of the coordinate's values of that polygon
		"""
		
		result = ""
		for i in array.points:
			result += "{} {} ".format(i.x, i.y)
		return result[:-1]
	
	def __init__(self, environment: Environment, encoding: str = "UTF-8"):
		"""
		Constructs a Generator object that will be able to produce
		SVG images that show the way to go for Lisa

		:param environment: Environment object that should be "plotted"
		:param encoding: encoding used for reading & writing files
		"""
		
		# Encoding used to read & write files to disk
		self.encoding = encoding
		
		# Environment that will be used to generate output data
		self.environment = environment
		
		# Number of additionally added lines
		self.lines = 0
	
	def update(self, environment: Environment) -> object:
		"""
		Updates the environment information stored in this Generator
		
		:param environment: a full environment
		:return: self (the instance of the Generator class)
		"""
		
		if isinstance(environment, Environment):
			self.environment = environment
		return self
	
	def produce(self, coordinates: str, color: str = "#080000") -> str:
		"""
		Returns a single polyline string to be inserted in a SVG image

		:param coordinates: string of correctly formatted coordinates
		:param color: full hex RGB color for the line
		:return: string that contains exactly one polyline
		"""
		
		return ImageGenerator.TemplateLine.format(
			coordinates = coordinates,
			color = color,
			lineID = "L" + str(self.lines),
			stroke = 4
		)
	
	def generate(self, lines: list = list()) -> str:
		"""
		Generates a valid SVG output image and returns it
		
		This method requires self.environment to be set up properly!
		
		The list of lines has to be a list of lists of tuples (coordinates).
		Those are inserted at the end of the SVG image creation in
		a dark green color (#008000) by default.
		
		Example:
			lines = [
				[
					(3,6),
					(6,8),
					(0,2)
				],
				[
					(0,0),
					(1,1),
					(4,1),
					(0,0)
				]
			]
		
		:return: valid SVG image showing the current environment as string
		"""
		
		# Generate the list of polygons' paths
		polygons = [
			ImageGenerator.TemplatePolygon.format(
				points = p
			) for p in [
				ImageGenerator.transform(i)
				for i in self.environment.graph.polygons
			]
		]
		
		# Generate the string containing all information for the lines
		extras = ""
		for line in range(len(lines)):
			extras += "\n" + ImageGenerator.TemplateLine.format(
				lineID = line,
				coordinates = "",
				color = "#008000",
				stroke = 2
			)
		
		# Get the size of the environment
		w, h = DEFAULT_MAP_SIZE
		if self.environment.size[0] != -1:
			w = self.environment.size[0]
			log.debug("Changed total image width to", w)
		if self.environment.size[1] != -1:
			h = self.environment.size[1]
			log.debug("Changed total image height to", h)
		
		# Return the fully formatted (filled) SVG image string
		return ImageGenerator.TemplateImage.format(
			polygons = "\n".join(polygons),
			path = " ".join([
				"{} {}".format(i.x, i.y) for i in self.environment.path
			]),
			lisaX = self.environment.start.x,
			lisaY = self.environment.start.y,
			lines = extras[1:],
			TotalWidth = w,
			TotalHeight1 = h,
			TotalHeight2 = h,
			TotalHeight3 = h
		)
	
	def write(self, path: str, mode: str = "w", force: bool = False) -> int:
		"""
		Generates a complete SVG file and writes it to the given path

		:param path: path to the destination file
		:param mode: mode used to open I/O buffer (must be writable)
		:param force: allow overwriting existing files
		:return: number of written bytes
		"""
		
		# Check if the output file exists and if it's safe to write to it
		if os.path.exists(path) and force or not os.path.exists(path):
			
			# Save the output to the file and return
			with open(path, mode, encoding = self.encoding) as file:
				size = file.write(self.generate())
			return size
		
		# Now the file exists and it's not allowed to write to it!
		log.error('File "{}" already exists!'.format(path))
		log.info("If you want to allow overwriting it, run again with -f.")
		return 0


class WorldGenerator:
	"""
	Generator for random maps (environments) to test the solving feature
	"""
	
	MapRE = "^[0-9]+/[0-9]+,[0-9]+@[0-9]+-[0-9]+,[0-9]+/[0-9]+$"
	
	def __init__(self, cmd: str):
		"""
		Constructs the WorldGenerator object and checks the specified cmd
		
		:param cmd: string containing the map specifications
		"""
		
		# Check for the matching pattern
		match = re.fullmatch(self.MapRE, cmd)
		if match is None or match.pos != 0:
			log.error("Map spec doesn't match the regular expression!")
			raise RuntimeError("Pattern-matching failed.")
		else:
			log.debug("Found a valid map specification.")
		
		# Extract all the data from the map spec string (all type
		# conversions should be safe due to the regular expression)
		categories = cmd.split(",")
		self.size = tuple([int(i) for i in categories[0].split("/")])
		v, p = categories[1].split("@")
		self.vertices = int(v)
		self.polygons = tuple([int(i) for i in p.split("-")])
		self.start = tuple([int(i) for i in categories[2].split("/")])
	
	def build(self) -> Environment:
		"""
		Builds up the environment and returns it
		
		:return: fully filled Environment containing all necessary data
		"""
		
		raise NotImplementedError
