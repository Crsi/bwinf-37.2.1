#!/usr/bin/env python3

"""
Path finding trough 2D maps based on visibility graphs

This module is used as a script (so test calling it with option -h).
It only exports a function called `main` which is usually called
with sys.argv and handles the whole user interaction. Note that
this module handles the user interface and therefore prints to
sys.stdout and sys.stderr.

The program first reads the input file or map specification and
generates a so-called `Environment`. This contains a list of
"obstacles". Those obstacles are polygons (implemented as lists
of vertices) which can not be passed by a runner along the path.
Then, a full visibility graph will be generated. This graph
maintains a set of vertices, the list of polygons and a blocking
(non-adjacency) matrix. This matrix contains information which
vertices can not be seen from a specific Vertex. The graph itself
is not directed and not weighted. Note that generating the
visibility graph takes most of the program's running time.

The program uses a pathfinder algorithm based on a recursive
depth-first algorithm and backtracking combined with scored
Vertex testing for better performance.

Additional features of the program include verbosity
configuration, persistent Environment storing, SVG image
output and randomly generated maps.
"""

import os
import sys
import argparse

import log
import reader
import generator
import processor
from constants import *


def _setup() -> argparse.ArgumentParser:
	"""
	Sets up the command-line interface

	:return: ArgumentParser object
	"""
	
	# Create the very important parser object
	parser = argparse.ArgumentParser(
		description = __doc__,
		epilog = "This is the solution to BwInf 37.2.1",
		formatter_class = argparse.RawDescriptionHelpFormatter,
		add_help = False
	)
	
	# Add the argument for the input file / map specs
	parser.add_argument_group("Input").add_argument(
		"input",
		type = str,
		metavar = "<file/spec>",
		help = "input file or map spec (see below)"
	)
	
	# Add arguments to handle the program output (image files)
	output = parser.add_argument_group(
		"Output",
		"It's possible to change the output of results into SVG image\n"
		"files. Specifying a path for the option -o will safe a final\n"
		"image showing the environment and a path to the destination\n"
		"together with the start point and all obstacles. For even more\n"
		"output and understanding algorithms, adding the option -c will\n"
		"generate those SVG images after every step taken by the program.\n"
		"When a file exists, the program usually doesn't touch it, as\n"
		"long as the option -f is not given which allows overwriting files."
	)
	output.add_argument(
		"-c",
		action = "store_true",
		dest = "continuous",
		help = "enable continuous output of the current program's state"
	)
	output.add_argument(
		"-f",
		action = "store_true",
		dest = "force",
		help = "allow overwriting existing files"
	)
	output.add_argument(
		"-o",
		type = str,
		dest = "output",
		metavar = "<file>",
		default = "",
		help = "path to the output image file"
	)
	
	# Add arguments to change the verbosity of the console output
	verbosity = parser.add_argument_group(
		"Verbosity",
		"It's possible to set the verbosity of the program log\n"
		"by using one of this three switches here. Note that\n"
		"the progress bar uses third-party libraries and may not\n"
		"work properly. Option -p uses normal verbosity."
	).add_mutually_exclusive_group()
	verbosity.add_argument(
		"-p",
		action = "store_true",
		dest = "progress",
		help = "show the program's progress using a fancy progressbar"
	)
	verbosity.add_argument(
		"-q",
		action = "store_true",
		dest = "quiet",
		help = "mute the program to suppress printing logs"
	)
	verbosity.add_argument(
		"-v",
		action = "store_true",
		dest = "verbose",
		help = "enable the output of debugging messages"
	)
	
	# Add arguments to handle persistent storage
	storage = parser.add_argument_group(
		"Storage",
		"It's possible to load and safe environment files that\n"
		"allow fast rebuild of existing visibility graphs, so that\n"
		"you can speed up the whole process enormously.\n"
		"Note that you can't change the environment heavily after loading\n"
		"something from a storage file. Setting the start point is supported."
	)
	storage.add_argument(
		"-l",
		type = str,
		dest = "load",
		metavar = "<store>",
		default = "",
		help = "path to the storage file"
	)
	storage.add_argument(
		"-s",
		type = str,
		dest = "save",
		metavar = "<store>",
		default = "",
		help = "path to the storage file"
	)
	
	# Add arguments to call the help page
	helper = parser.add_argument_group(
		"Help",
		"It's possible that the program only shows this help message."
	).add_mutually_exclusive_group()
	helper.add_argument(
		"-h",
		action = "help",
		help = "show this help message and exit"
	)
	helper.add_argument(
		"-?",
		action = "help",
		help = "show this help message and exit"
	)
	
	# By using the group feature, it produces
	# beautifully formatted help messages
	parser.add_argument_group(
		"<file/spec>",
		"Provide a path to a valid input file here!\n"
		"Alternatively it's possible to provide a map generator spec.\n"
		"This is a string that may look a bit strange but provides all\n"
		"information needed by the map generator to build a unique, random\n"
		"map that can be used later on to build a graph in it. The string\n"
		"provided as map spec has to match the following regular expression:\n"
		"\n\t^[0-9]+/[0-9]+,[0-9]+@[0-9]+-[0-9]+,[0-9]+/[0-9]+$\n\n"
		"So, you have to specify the width and height of the whole map\n"
		"separated by a slash at first, followed by a comma and the number\n"
		"of nodes all the obstacles will have together. After the @ symbol,\n"
		"there's the range of number of obstacles, separated by a minus sign.\n"
		"It follows a comma to separate it from the position of the start\n"
		"point that is expressed using the coordinate scheme as for the\n"
		"size of the map: two numbers separated by a slash.\n\n"
		"This example provides a map that is 512 meters long and 128 meters\n"
		"wide. It contains exactly 32 nodes that are divided into obstacles\n"
		"with 4 to 8 nodes each. So, the number of obstacles is variable.\n"
		"The start point for this example is at the position 256/64 which\n"
		"is the exact middle of the example map.\n"
		"The code to this example is:\n\n\t"
		"512/128,32@4-8,256/64"
	)
	
	# Return the parser (not the parsed results!) for better reuse
	return parser


def main(arguments: list = sys.argv) -> int:
	"""
	Runs the main program

	:param arguments: list of arguments from the command line (sys.argv)
	:return: success status (hopefully 0)
	"""
	
	# Allow specifying the --help switch that is a default switch
	# for most command-line programs by replacing it with -h
	try:
		arguments.remove("--help")
	except ValueError:
		pass
	else:
		arguments.append("-h")
	
	# Setup the command-line interface and parse all the arguments
	args = _setup().parse_args(arguments[1:])

	# Set the correct "permissions" to show verbose/normal messages
	log.DEBUGGING = args.verbose
	log.LOGGING = not args.quiet
	log.debug("Program arguments:", args)
	
	# Stop the execution in case of currently unsupported options
	if args.load != "" or args.save != "":
		log.error(
			"Sorry! Currently, loading and saving environment files "
			"is not supported..."
		)
		sys.exit(2)
	
	# Check if the path exists, then read the provided
	# file to build the environment based on that data
	if os.path.exists(args.input):
		log.debug("Calling reader...")
		environment = reader.Reader.read(args.input)
		log.info("Successfully set up environment.")
		log.debug(str(environment))
	
	# Try creating an environment using the WorldGenerator
	else:
		log.info("File not found: \"{}\"".format(args.input))
		log.debug("Trying to use it as map spec ...")
		
		# Try matching the regular expression
		try:
			gen = generator.WorldGenerator(args.input)
		except RuntimeError:
			log.error("Aborting program execution.")
			sys.exit(2)
		
		# Set up the environment
		try:
			environment = gen.build()
		except NotImplementedError:
			log.error("This feature is not ready to be used. Exiting.")
			sys.exit(1)
		except ValueError:
			log.error("Conversion error occurred. Aborting.")
			sys.exit(2)
		log.info("Successfully set up environment.")
	
	# Find the naming scheme
	scheme = "{}{id}{}".format(*os.path.splitext(args.output), id = "-{id}")
	if args.continuous:
		log.debug("Saving continuously under", scheme)
		log.info(
			"Sorry, continuously saving files is currently not supported."
		)
	
	# Check the configuration of the pathfinder
	if (SCORE_DOWNWARDS + SCORE_UPWARDS) / 2 != 1.0:
		log.info("Warning: Unbalanced scoring multipliers!")
	
	# Execute the processor
	p = processor.Processor(environment, args)
	success = p.main()
	
	# Report the results
	log.debug("Main routine reported {}success!".format(
		"no " if not success else ""
	))
	
	# Check if SVG output should be generated
	if args.output != "":
		log.debug("Generating SVG output image ...")
		
		gen = generator.ImageGenerator(environment)
		size = gen.write(args.output, force = args.force)
		log.info('Wrote {} bytes to file "{}".'.format(size, args.output))
	
	# Show the environment if there's no SVG output
	else:
		log.info("No SVG output image generated.")
		log.debug("Current environment:", environment)
	
	# Calculate the information to create the final report
	distanceLisa = p.calc(environment.path)
	timeLisa = distanceLisa / SPEED_LISA
	distanceBus = environment.path[-1].y
	timeBus = distanceBus / SPEED_BUS
	
	# Output the full stats about the path
	log.info("Path to the destination: " + " ".join(
		["({}, {}),".format(i.x, i.y) for i in environment.path]
	)[:-1])
	log.debug("Raw path coordinates: " + " ".join(
		["{} {}".format(i.x, i.y) for i in environment.path]
	))
	log.info("Length of the path: {:.2f}m".format(
		distanceLisa
	))
	log.info(
		"The bus needs {:.1f}s for the distance of {}m. "
		"Lisa needs {:.1f}s to reach that point. Therefore, "
		"Lisa has to start {:.1f}s before "
		"the bus starts at 7:30!".format(
			timeBus, distanceBus, timeLisa, - timeBus + timeLisa
		)
	)
	
	# Finally close the program by returning 0 or 1
	if len(environment.path) > 0:
		log.debug("Finished.")
		return 0
	else:
		log.error("Sorry. No valid path was found. :-(")
		return 1


if __name__ == "__main__":
	sys.exit(main(sys.argv))
