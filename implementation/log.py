#!/usr/bin/env python3

"""
"""

import sys

# Enable or disable debugging logs
DEBUGGING = False

# Enable or disable normal output
LOGGING = True


def debug(*args, **kwargs) -> None:
	"""
	Writes a debug message to sys.stdout (as far as not specified otherwise)

	:param args: any arguments that will be used with "print"
	:param kwargs: any arguments that will be used with "print"
	:return: None
	"""
	
	if DEBUGGING and LOGGING:
		return print("[DBG]", *args, **kwargs)


def info(*args, **kwargs) -> None:
	"""
	Writes a log message to sys.stdout (as far as not specified otherwise)

	:param args: any arguments that will be used with "print"
	:param kwargs: any arguments that will be used with "print"
	:return: None
	"""
	
	if LOGGING:
		return print("[INF]", *args, **kwargs)


def error(*args, **kwargs) -> None:
	"""
	Writes an error message to sys.stderr

	:param args: any arguments that will be used with "print"
	:param kwargs: any arguments that will be used with "print"
	:return: None
	"""
	
	# Usually everything is safe here
	try:
		return print("[ERR]", *args, file = sys.stderr, **kwargs)
	
	# Detect the use of another "file" specification in the keyword args
	except TypeError:
		print(
			"Internal Error: used \"file\" keyword with log.error!",
			file = sys.stderr
		)
		return print("[ERR]", *args, file = sys.stderr)
