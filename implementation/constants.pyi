#!/usr/bin/env python3

# This file contains the default values for the configured constants

# Speed constants of the Bus and Lisa in m/s
SPEED_BUS: float = 30 / 3.6
SPEED_LISA: float = 15 / 3.6


# Multipliers for the scoring function in the path finder
# (a lower score is a better score)
SCORE_X: float = 1.0
SCORE_DISTANCE: float = 1.0

# Those two scores should be 2.0 together
SCORE_DOWNWARDS: float = 1.1
SCORE_UPWARDS: float = 0.9


# Limit the number of retries for the pathfinder
PATHFINDER_LIMIT: int = 8


# Specify the default map size as tuple of (width, height)
DEFAULT_MAP_SIZE: tuple = (1024, 768)
