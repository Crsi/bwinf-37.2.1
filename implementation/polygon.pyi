#!/usr/bin/env python3

from typing import Iterator

from vertices import Vertex


class Polygon:
	
	points: list
	id: int
	
	def __init__(self, vertices: list):
		...
	
	def __getitem__(self, i: int) -> Vertex:
		...
	
	def __iter__(self) -> Iterator:
		...
	
	def __len__(self) -> int:
		...
	
	def __str__(self) -> str:
		...
