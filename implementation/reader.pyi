#!/usr/bin/env python3

from typing import List, Union

from polygon import Polygon
from environment import Environment


class DataFormatError(Exception):
	...


class Reader:
	
	@classmethod
	def create(cls,	n: int,	values: List[Union[int, str]]) -> Polygon:
		...
	
	@classmethod
	def extract(cls, content: str) -> Environment:
		...
	
	@classmethod
	def read(cls, path: str, encoding: str = "UTF-8") -> Environment:
		...
