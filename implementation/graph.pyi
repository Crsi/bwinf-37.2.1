#!/usr/bin/env python3

import collections
from typing import List, Optional, Sequence, Set, Type, Union

from edge import Edge
from polygon import Polygon
from vertices import Vertex


GraphElement = Union[Vertex, Edge, Polygon]


class Graph:
	
	__slots__ = ["adjacency", "blocker", "polygons", "_currentPolygonID"]
	
	adjacency: collections.defaultdict
	blocker: collections.defaultdict
	polygons: List[Polygon]
	_currentPolygonID: int
	
	def __init__(self, polygons: List[Polygon]) -> Graph:
		...
	
	def __add__(self, obj: Union[GraphElement, List[GraphElement]]) -> None:
		...
	
	def __contains__(self, item: GraphElement) -> bool:
		...
	
	def __getitem__(self, item: Union[Vertex, int]) -> Union[Set[Vertex], Polygon]:
		...
	
	def __str__(self) -> str:
		...
	
	def __sub__(self, obj: GraphElement) -> None:
		...
	
	def _addVertex(self, vertex: Vertex) -> None:
		...
	
	def _addEdge(self, edge: Edge) -> None:
		...
	
	def _addPolygon(self, polygon: Polygon) -> None:
		...
	
	def _block(self, edge: Union[Sequence[Vertex], Edge]) -> bool:
		...
	
	def _popVertex(self, vertex: Vertex) -> Union[Vertex, None]:
		...
	
	def _popEdge(self, edge: Edge) -> Union[Edge, None]:
		...
	
	def _popPolygon(self, polygon: Polygon) -> Union[Polygon, None]:
		...
	
	def add(self, obj: Union[GraphElement, List[GraphElement]]) -> None:
		...
	
	def block(self, obj: Union[Sequence[Vertex], Edge]) -> bool:
		...
	
	def check(self, edge: Union[Sequence[Vertex], Edge]) -> bool:
		...
	
	def delete(self, obj: GraphElement) -> bool:
		...
	
	def free(self) -> List[Vertex]:
		...
	
	def get(self, t: Union[Type[Vertex], Type[Polygon]]) -> Union[List[Vertex], List[Polygon]]:
		...
	
	def isOriginal(self, edge: Union[Sequence[Vertex], Edge]) -> bool:
		...
	
	def next(self, point: Vertex) -> Set[Vertex]:
		...
	
	def pop(self, obj: GraphElement) -> Optional[GraphElement]:
		...
	
	def verify(self, start: Vertex, end: Vertex) -> bool:
		...

