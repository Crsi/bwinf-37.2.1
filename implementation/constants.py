#!/usr/bin/env python3

"""
Constants used by other modules of the project

It's usually safe to edit the values here and see what happens!
"""

# Speed constants of the Bus and Lisa in m/s
SPEED_BUS = 30 / 3.6
SPEED_LISA = 15 / 3.6


# Multipliers for the scoring function in the path finder
# (a lower score is a better score)
SCORE_X = 1.0
SCORE_DISTANCE = 1.0
SCORE_DOWNWARDS = 1.1
SCORE_UPWARDS = 0.9


# Limit the number of retries for the pathfinder
PATHFINDER_LIMIT = 8


# Specify the default map size as tuple of (width, height)
DEFAULT_MAP_SIZE = (1024, 768)
