#!/usr/bin/env python3

"""
"""

import math
from typing import Iterator, Tuple, Union

from vertices import Vertex


class Edge:
	"""
	Edge representing a "free" way from one to another Vertex
	
	Note that there is no comparision of the both vertices
	of the Edge, aka the graph containing the edges is not directed.
	The distance is calculated once at initialization because
	usually the Edge's points won't be changed afterwards.
	"""
	
	__slots__ = ["a", "b", "distance"]
	
	def __init__(self, a: Vertex, b: Vertex):
		"""
		Initializes the Edge object
		
		:param a: Vertex at the one point
		:param b: Vertex at the other point
		"""
		
		self.a = a
		self.b = b
		
		self.distance = math.sqrt(
			(a.x - b.x) ** 2 + (a.y - b.y) ** 2
		)
	
	def __contains__(self, item: Vertex) -> bool:
		"""
		Returns whether the Edge has a Vertex that equals the specified
		
		:param item: Vertex of a graph or similar
		:return: whether item equals one of both vertices
		"""
		
		return item == self.a or item == self.b
	
	def __getitem__(self, item: int) -> Vertex:
		"""
		Returns the
		
		:param item:
		:return:
		"""
		
		if item == 0:
			return self.a
		elif item == 1:
			return self.b
		else:
			raise ValueError("Unexpected item:", item)
	
	def __iter__(self) -> Iterator:
		"""
		Returns an iterator over both vertices A and B
		
		:return: iterator over [A, B]
		"""
		
		return iter([self.a, self.b])
	
	def __repr__(self) -> str:
		"""
		Returns repr(self)
		
		:return: string representation of self
		"""
		
		return "Edge({} -> {})".format(self.a, self.b)
	
	def get(self, v: Vertex) -> Union[Vertex, None]:
		"""
		Returns the "other" Vertex contained in the Edge or None
		
		:param v: Vertex whose "neighbor" should be searched
		:return: "other" Vertex or None if point is not part of the Edge
		"""
		
		if v == self.a:
			return self.b
		elif v == self.b:
			return self.a
		return None
		
	def hits(self, point: Tuple[float, float]) -> bool:
		"""
		Returns whether the point lies somewhere in the range of the Edge
		
		This method does only verify that the point is part of the
		line segment (Edge), not only the line. Any point that is compared
		here is assumed to lie on the line through A and B of the Edge.
		
		:param point: tuple of coordinates (2D point)
		:return: boolean whether the point could be inside the Edge
		"""
		
		return (
			min(self.a.x, self.b.x) <= point[0] <= max(self.a.x, self.b.x)
			and
			min(self.a.y, self.b.y) <= point[1] <= max(self.a.y, self.b.y)
		)
