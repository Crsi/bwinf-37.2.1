#!/usr/bin/env python3

"""
"""

from typing import List, Union

import log
from vertices import Vertex
from polygon import Polygon
from environment import Environment


class DataFormatError(Exception):
	"""
	Exception for wrong data input formats
	"""


class Reader:
	"""
	Reader for example files containing given polygons
	and the position of Lisa's house
	
	The returned value of the functions "extract" and "read"
	is an object of the Environment class. See its
	docstrings for further information.
	"""
	
	@classmethod
	def create(cls,	n: int,	values: List[Union[int, str]]) -> Polygon:
		"""
		Returns a Polygon made of the values from the list of coordinates
		
		Syntax of the input list of values:
			[x1, y1, x2, y2, x3, y3, ..., xN, yN]
		The coordinates may be integers or strings but no floats.
		
		:param n: number of pairs of coordinates in the set of values
		:param values: pairs of coordinates that describe the polygon
		:return: Polygon object containing all vertices
		"""
		
		if 2 * n != len(values):
			raise ValueError("Incorrect number of points:", n)
		
		result = []
		for each in range(n):
			
			# Convert the (possibly) strings to integers
			try:
				x = int(values[2 * each])
				y = int(values[2 * each + 1])
			except ValueError:
				raise DataFormatError("Coordinates are no integers!")
			
			# Create a Vertex at the desired position
			result.append(Vertex(x, y))
		
		# Return the Polygon as obstacle representation
		return Polygon(result)
	
	@classmethod
	def extract(cls, content: str) -> Environment:
		"""
		Reads the Environment from a given string

		:param content: raw string with the data
		:return: fully "filled" Environment
		"""
		
		# Initialize the size value and start a loop over all lines
		size = 0
		result = []
		x = y = 0
		for line in content. \
			replace("\r\n", "\n"). \
			replace("\r", "\n"). \
			split("\n"):
			
			# In the first line of content, size wasn't set yet
			if size == 0:
				try:
					size = int(line)
				except ValueError:
					raise DataFormatError(
						"{} does not contain a valid number".format(line)
					)
			
			# Avoid empty lines
			elif line == "":
				continue
			
			# Split the lines into several "words"
			else:
				words = line.split(" ")
				
				# Check if there're only two words
				# (this are the coordinates of the house!)
				if len(words) == 2:
					try:
						x = int(words[0])
						y = int(words[1])
					except ValueError:
						raise DataFormatError(
							"{} or {} is not an integer".format(
								words[0], words[1]
							)
						)
				
				else:
					
					# Get the number of points for the polygon
					try:
						n = int(words[0])
					except ValueError:
						raise DataFormatError(
							"{} is not a number".format(words[0])
						)
					
					# Add the newly created array to the list of polygons
					result.append(cls.create(n, words[1:]))
		
		# Check if the coordinates were set
		if x == 0 or y == 0:
			raise DataFormatError("Lisa's house was not defined correctly")
		else:
			log.debug("Start position set to", (x, y))
		
		return Environment(
			result,
			Vertex(x, y)
		)
	
	@classmethod
	def read(cls, path: str, encoding: str = "UTF-8") -> Environment:
		"""
		Reads the environment from file

		:param path: path to the file
		:param encoding: encoding used for it (defaults to UTF-8)
		:return: fully "filled" Environment
		"""
		
		file = open(path, "r", encoding = encoding)
		content = file.read()
		file.close()
		
		return cls.extract(content)
