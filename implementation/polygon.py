#!/usr/bin/env python3

"""
"""

from typing import Iterator

from vertices import Vertex


class Polygon:
	"""
	Polygon representing the data structure to loosely store all
	Vertices of an obstacle
	"""
	
	def __init__(self, vertices: list):
		"""
		Produces the Polygon object
		
		Note that all points (Vertex objects) should have been created
		when constructing the Polygon object!
		"""
		
		# List of Vertices
		self.points = vertices
		
		# Polygon ID used to identify it in the graph
		self.id = -1
	
	def __getitem__(self, i: int) -> Vertex:
		"""
		Returns the i-th element of the internal list of points
		
		:param i: index of the internal list of points
		:return: Vertex at position i
		"""
		
		return self.points[i]
	
	def __iter__(self) -> Iterator:
		"""
		Returns an iterator over the vertices of the Polygon
		
		:return: iterator over all objects of type Vertex
		"""
		
		return iter(self.points)
	
	def __len__(self) -> int:
		"""
		Returns the number of nodes of this obstacle
		
		:return: len(self.nodes)
		"""
		
		return len(self.points)
	
	def __str__(self) -> str:
		"""
		Returns a valid string representing the filled Polygon
		
		:return: string representation of self
		"""
		
		return "Polygon({})".format(self.points)
