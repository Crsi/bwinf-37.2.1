#!/usr/bin/env python3

import argparse
from typing import List, Optional

from graph import Graph
from vertices import Vertex
from generator import ImageGenerator
from environment import Environment


class Processor:
	
	environment: Environment
	args: argparse.Namespace
	generator: ImageGenerator
	step: int = 0
	
	@classmethod
	def calc(cls, path: List[Vertex]) -> float:
		...
	
	def __init__(self, environment: Environment, args: argparse.Namespace) -> Processor:
		...
	
	def nextStep(self, start: Vertex, graph: Graph, escape: int = 0, destination: Optional[Vertex] = None) -> List[Vertex]:
		...
	
	def optimize(self, path: List[Vertex]) -> List[Vertex]:
		...
	
	def main(self) -> bool:
		...
