#!/usr/bin/env python3

from typing import Iterator, Tuple, Union

from vertices import Vertex


class Edge:
	
	__slots__ = ["a", "b", "distance"]
	
	# An edge is made of two vertices (however, they are not ordered!)
	a: Vertex
	b: Vertex
	
	# The distance may be float or int if applicable
	distance: Union[int, float]
	
	def __init__(self, a: Vertex, b: Vertex) -> Edge:
		...
	
	def __contains__(self, item: Vertex) -> bool:
		...
	
	def __getitem__(self, item: int) -> Vertex:
		...
	
	def __iter__(self) -> Iterator:
		...
	
	def __repr__(self) -> str:
		...
	
	def get(self, v: Vertex) -> Union[Vertex, None]:
		...
	
	def hits(self, point: Tuple[float, float]) -> bool:
		...
