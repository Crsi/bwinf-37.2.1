#!/usr/bin/env python3

from typing import List, Tuple

from polygon import Polygon
from environment import Environment


class ImageGenerator:
	
	TemplateLine: str
	TemplatePolygon: str
	TemplateImage: str
	
	encoding: str
	environment: Environment
	lines: int
	
	@classmethod
	def transform(cls, array: Polygon) -> str:
		...
	
	def __init__(self, environment: Environment, encoding: str = "UTF-8") -> ImageGenerator:
		...
	
	def update(self, environment: Environment) -> ImageGenerator:
		...
	
	def produce(self, coordinates: str, color: str = "#080000") -> str:
		...
	
	def generate(self, lines: List[List[Tuple[int, int]]] = list()) -> str:
		...
	
	def write(self, path: str, mode: str = "w", force: bool = False) -> int:
		...

class WorldGenerator:
	
	# Static regular expression that has to be matched
	MapRE: str
	
	# Range of number of generated polygons
	polygons: Tuple[int, int]
	
	# Size of the whole area in x/y coordinates
	size: Tuple[int, int]
	
	# Start position (Lisa's house) in x/y coordinates
	start: Tuple[int, int]
	
	# Total number of vertices that should be generated
	vertices: int
	
	def __init__(self, cmd: str) -> WorldGenerator:
		...
	
	def build(self) -> Environment:
		...
