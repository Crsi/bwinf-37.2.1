#!/usr/bin/env python3

from typing import Sequence, Union


class Vertex:
	
	__slots__ = ["x", "y", "free", "id"]
	
	# Coordinates of the point
	x: int
	y: int
	
	# Flag if the y-axis (destination) is directly reachable
	free: bool
	
	# Reference to the Polygon ID used to identify it in the graph
	id: int
	
	def __init__(self, x: Union[Sequence[int], int], y: int = None):
		...
	
	def __getitem__(self, index: int) -> int:
		...
	
	def __hash__(self) -> int:
		...
	
	def __repr__(self) -> str:
		...
	
	def __lt__(self, obj: Vertex) -> bool:
		...

	def __eq__(self, obj: Vertex) -> bool:
		...
