#!/usr/bin/python3

import os
from typing import Any, Iterable, List, Tuple, Optional

from vertices import Vertex
from polygon import Polygon
from graph import Graph


def progressAlternative(iterable: Iterable, *args: Any, **kwargs: Any) -> Iterable:
	...


try:
	from tqdm import tqdm
except ImportError:
	tqdm = progressAlternative


def tab(text: str, indent: str = "\t", linesep: str = os.linesep) -> str:
	...


class Environment:
	
	__slots__ = ["graph", "start", "path", "size"]
	
	graph: Graph
	start: Optional[Vertex]
	path: List[Vertex]
	size: Tuple[int, int]
	
	def __init__(self, polygons: List[Polygon], start: Optional[Vertex] = None):
		...
	
	def __str__(self) -> str:
		...
	
	def build(self, progressbar: bool = False) -> bool:
		...


def load(pathname: str) -> Environment:
	...


def save(pathname: str, env: Environment) -> int:
	...
