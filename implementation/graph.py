#!/usr/bin/env python3

"""
"""

import collections
from typing import List, Optional, Sequence, Set, Type, Union

import log
from edge import Edge
from polygon import Polygon
from vertices import Vertex


GraphElement = Union[Vertex, Edge, Polygon]


class Graph:
	"""
	Graph representing a container for all vertices, edges and polygons
	
	The Graph also stores the visibility information between vertices.
	For this purpose the attribute `adjacency` manages an associative
	array where the keys are Vertex objects and the values are sets
	of Edges that contain that key (Vertex). When resetting (clearing)
	the Graph, this adjacency information together with the content of
	the attribute `edges` that stores a set of all Edges will be removed.
	"""
	
	__slots__ = ["adjacency", "blocker", "polygons", "_currentPolygonID"]
	
	def __init__(self, polygons: List[Polygon]):
		"""
		Initializes the Graph object
		
		:param polygons: list of all polygons known to the graph
		"""
		
		# Adjacency dictionary where the keys are Vertices and
		# the values are sets of Vertices visible by the key
		# This data structure is only generated at initialization and
		# should not be modified later as it stores the original edges
		self.adjacency = collections.defaultdict(set)
		
		# Blocker dictionary where the keys are Vertices and
		# the values are sets of Vertices NOT visible by the key
		# This data structure is meant to be modified during the
		# calculation of the visibility graph
		self.blocker = collections.defaultdict(set)
		
		# List of all polygons
		self.polygons = []
		
		# Maintain a counter for the polygons
		self._currentPolygonID = -1
		
		# Initialize the graph with the currently available polygons
		self.add(polygons)
		
	def __add__(self, obj: Union[GraphElement, List[GraphElement]]) -> None:
		"""
		Adds the object to the Graph and rebuilds all necessary data
		
		Depending on which type of data was given, the method
		handles the cases differently. Note that it's possible to
		break the whole graph by adding stupid Edges for example!
		After adding the new material, all necessary steps for
		rebuilding the necessary data will be taken.
		
		:param obj: Vertex/Edge/Polygon object
		:return: None
		"""
		
		return self.add(obj)
	
	def __contains__(self, item: GraphElement) -> bool:
		"""
		Checks whether the graph contains the given object
		
		:param item: Vertex/Edge/Polygon object
		:return: whether the given item is part of any data structure
		"""
		
		return item in self.get(type(item))
	
	def __getitem__(
		self, item: Union[Vertex, int]
	) -> Union[Set[Vertex], Polygon]:
		"""
		Returns the requested item set
		
		If an integer is given, this method returns the Polygon that
		has that ID. If a Vertex is given, it returns a set of all
		vertices that can still be seen by that Vertex at the moment.
		
		:param item: integer (Polygon ID) or Vertex
		:return: Polygon or list of adjacent vertices
		"""
		
		if isinstance(item, Vertex):
			return set(self.adjacency.keys()) - self.blocker[item]
		elif isinstance(item, int):
			for each in self.polygons:
				if each.id == item:
					return each
			raise ValueError("Unknown Polygon ID:", item)
		else:
			raise TypeError("Unexpected type:", type(item))
	
	def __str__(self) -> str:
		"""
		Returns a readable & printable string representation of the graph
		
		:return: string representation of the graph
		"""
		
		raise NotImplementedError
	
	def __sub__(self, obj: GraphElement) -> None:
		"""
		Deletes the object from the Graph

		:param obj: Vertex/Edge/Polygon object
		:return: None
		"""
		
		self.delete(obj)
	
	def _addVertex(self, vertex: Vertex) -> None:
		"""
		Adds the specified Vertex to the Graph
		
		:param vertex: Vertex object that may already be part of the Graph
		:return: None
		"""
		
		# Adding the Vertex is really simple because of the used data type
		self.adjacency[vertex]
		self.blocker[vertex]
		
	def _addEdge(self, edge: Edge) -> None:
		"""
		Adds the specified Edge to the Graph
	
		:param edge: Edge object that may already be part of the Graph
		:return: None
		"""
		
		# It's not checked if the Edge exists, because it's used with sets
		self.adjacency[edge.a].add(edge.b)
		self.adjacency[edge.b].add(edge.a)
	
	def _addPolygon(self, polygon: Polygon) -> None:
		"""
		Adds the specified Polygon to the Graph

		:param polygon: Polygon object that may already be part of the Graph
		:return: None
		"""
		
		if polygon not in self.polygons:
			polygon.id = self._currentPolygonID = self._currentPolygonID + 1
			self.polygons.append(polygon)
		
		for i, v in enumerate(polygon.points):
			self._addVertex(v)
			v.id = polygon.id
			try:
				self._addEdge(Edge(polygon[i], polygon[i+1]))
			except IndexError:
				self._addEdge(Edge(polygon[i], polygon[0]))
	
	def _block(self, edge: Union[Sequence[Vertex], Edge]) -> bool:
		"""
		Adds a block for the specified Edge
		
		Note that you can't block an original Edge!
		
		:param edge: Edge object or a list of exactly two Vertices
		:return: previous state of the Edge
		"""
		
		if self.isOriginal(edge):
			return True
		
		old = self.check(edge)
		
		try:
			self.blocker[edge[0]].add(edge[1])
		except KeyError:
			pass
		
		try:
			self.blocker[edge[1]].add(edge[0])
		except KeyError:
			pass
		
		return old
	
	def _popVertex(self, vertex: Vertex) -> Union[Vertex, None]:
		"""
		Removes and returns the specified Vertex from the Graph

		:param vertex: Vertex object that should be popped
		:return: Vertex object (None if it's not part of the Graph)
		"""
		
		if vertex not in self.adjacency.keys():
			return None
		
		for v in self.adjacency.keys():
			try:
				self.adjacency[v].remove(vertex)
			except KeyError:
				continue
		
		del self.adjacency[vertex]
		return vertex
	
	def _popEdge(self, edge: Edge) -> Union[Edge, None]:
		"""
		Removes and returns the specified Edge from the Graph

		:param edge: Edge object that should be popped
		:return: Edge object (None if it's not part of the Graph)
		"""
		
		s = "{} had no connection to {} but removed anyway!"
		
		try:
			self.adjacency[edge.a].remove(edge.b)
		except KeyError:
			log.debug(s.format(edge.a, edge.b))
		
		try:
			self.adjacency[edge.b].remove(edge.a)
		except KeyError:
			log.debug(s.format(edge.b, edge.a))
		
		return edge
	
	def _popPolygon(self, polygon: Polygon) -> Union[Polygon, None]:
		"""
		Removes and returns the specified Polygon from the Graph
	
		:param polygon: Polygon object that should be popped
		:return: Polygon object (None if it's not part of the Graph)
		"""
		
		if polygon not in self.polygons:
			return None
		
		for v in polygon.points:
			self._popVertex(v)
		
		self.polygons.remove(polygon)
		return polygon
	
	def add(self, obj: Union[GraphElement, List[GraphElement]]) -> None:
		"""
		Adds the object to the Graph and rebuilds all necessary data

		Depending on which type of data was given, the method
		handles the cases differently. Note that it's possible to
		break the whole graph by adding stupid Edges for example!
		After adding the new material, all necessary steps for
		rebuilding the necessary data will be taken.

		:param obj: Vertex/Edge/Polygon object
		:return: None
		"""
		
		if isinstance(obj, Vertex):
			log.debug("Adding Vertex {} to Graph...".format(obj))
			self._addVertex(obj)
		
		elif isinstance(obj, Edge):
			log.debug("Adding Edge {} to Graph...".format(obj))
			self._addEdge(obj)
		
		elif isinstance(obj, Polygon):
			log.debug("Adding Polygon {} to Graph...".format(obj))
			self._addPolygon(obj)
		
		elif isinstance(obj, list):
			log.debug("Attempting to add list of objects to Graph...")
			for element in obj:
				self.add(element)
		
		else:
			raise TypeError("Unexpected type:", type(obj))
	
	def block(self, obj: Union[Sequence[Vertex], Edge]) -> bool:
		"""
		Adds a block for the specified Edge
		
		:param obj: Edge object or a list of exactly two Vertices
		:return: previous state of the Edge
		"""
		
		if isinstance(obj, Edge):
			return self._block(obj)
		
		elif isinstance(obj, (list, tuple)):
			if len(obj) != 2:
				raise ValueError("Expected length of 2!")
			
			return self._block(obj)
		
		else:
			raise TypeError("Unexpected type:", type(obj))
	
	def check(self, edge: Union[Sequence[Vertex], Edge]) -> bool:
		"""
		Returns the state of the Edge
		
		The state of an Edge describes if the Edge is "possible".
		If no vertex of the Edge is mentioned in one of their blockers,
		then the state is True, False otherwise.
		
		:param edge: Edge object
		:return: current state of the Edge
		"""
		
		return (
			edge[0] not in self.blocker[edge[1]]
			and edge[1] not in self.blocker[edge[0]]
		)
	
	def delete(self, obj: GraphElement) -> bool:
		"""
		Deletes the object from the Graph
		
		:param obj: Vertex/Edge/Polygon object
		:return: success of the operation
		"""
		
		return self.pop(obj) is not None
	
	def free(self) -> List[Vertex]:
		"""
		Returns a list of all known "free" vertices
		
		:return: list of vertices marked as "free"
		"""
		
		free = []
		vertices = list(self.adjacency.keys())
		for i in vertices:
			if i.free:
				free.append(i)
		return free
	
	def get(
		self, t: Union[Type[Vertex], Type[Polygon]]
	) -> Union[List[Vertex], List[Polygon]]:
		"""
		Returns all known objects of the requested type
		
		:param t: type of which all known objects should be returned
		:return: list of vertices / set of edges / list of polygons
		"""
		
		if t == Vertex:
			results = list(self.adjacency.keys())
			results.sort()
			return results
		elif t == Polygon:
			return self.polygons
		else:
			raise TypeError("Unexpected type:", t)
	
	def isOriginal(self, edge: Union[Sequence[Vertex], Edge]) -> bool:
		"""
		Returns whether the specified Edge is original
		
		:param edge: Edge object that should be part of the Graph
		:return: whether the Edge was set up as part of a Polygon
		"""
		
		return (
			edge[0] in self.adjacency[edge[1]]
			and edge[1] in self.adjacency[edge[0]]
		)
	
	def next(self, point: Vertex) -> Set[Vertex]:
		"""
		Returns a set of all vertices reachable by the given point
		
		:param point: Vertex contained in the Graph
		:return: set of adjacent vertices that are not blocked
		"""
		
		return self.adjacency[point] | self[point]
	
	def pop(self, obj: GraphElement) -> Optional[GraphElement]:
		"""
		Pops the object from the Graph
		
		:param obj: Vertex/Edge/Polygon object
		:return: specified object or None if doesn't exist
		"""
		
		if isinstance(obj, Vertex):
			log.debug("Removing Vertex {} from Graph...".format(obj))
			return self._popVertex(obj)
		
		elif isinstance(obj, Edge):
			log.debug("Removing Edge {} from Graph...".format(obj))
			return self._popEdge(obj)
		
		elif isinstance(obj, Polygon):
			log.debug("Removing Polygon {} from Graph...".format(obj))
			return self._popPolygon(obj)
		
		else:
			raise TypeError("Unexpected type:", type(obj))
	
	def verify(self, start: Vertex, end: Vertex) -> bool:
		"""
		Verifies that the line from the start to the end doesn't hit a Polygon
		
		If a line from a start Vertex S to an end Vertex E goes through a
		Polygon, this has one of two reasons:
			1. S and E are directly connected (they are members of the
			other's adjacency list)
			2. S and E are not directly connected (they aren't members
			of the other's adjacency list)
		
		In the first situation, everything is fine and `verify` returns True.
		But in the second case the line hits actually the inside of the
		Polygon and that's not allowed, so `verify` returns False.
		
		If S and E are NOT on the same polygon, then this returns True!
		
		:param start: start Vertex (could also be end)
		:param end: end Vertex (could also be start)
		:return: whether the line is "passable"
		"""
		
		if start.id == end.id:
			return (
				start in self.adjacency[end]
				and end in self.adjacency[start]
			)
		return True
