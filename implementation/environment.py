#!/usr/bin/env python3

"""
"""

import os
from typing import Any, List, Optional

import log
import visibility
from vertices import Vertex
from polygon import Polygon
from graph import Graph


def progressAlternative(iterable, *args: Any, **kwargs: Any):
	return iterable


try:
	from tqdm import tqdm
except ImportError:
	log.error("Importing some modules failed. No progressbar available.")
	
	tqdm = progressAlternative


def tab(text: str, indent: str = "\t", linesep: str = os.linesep) -> str:
	"""
	Returns a text where every line is intended by the <tab> character(s)
	
	:param text: original text which should be intended
	:param indent: character(s) to be inserted in front of every line
	:param linesep: line separator (defaults to os.linesep)
	:return: indented text
	"""
	
	return indent + (linesep + indent).join(text.split(linesep))


class Environment:
	"""
	Environment describing a full test case
	"""
	
	__slots__ = ["graph", "start", "path", "size"]
	
	def __init__(
		self, polygons: List[Polygon],
		start: Optional[Vertex] = None
	):
		"""
		Constructs a Environment object
		
		:param polygons: list Polygons to initialize the Graph
		:param start: optional Vertex holding the x/y position of Lisa's house
		"""
		
		# List of obstacles (usually not changed after initialization)
		self.graph = Graph(polygons)
		
		# Start vertex that should be part of the graph
		self.start = start
		if self.start not in self.graph:
			self.graph.add(self.start)
		
		# List of vertices from the start to the y-axis
		self.path = []
		
		# Set up the size of the environment
		# Note that (-1, -1) is a special case which means that the size
		# is unspecified and should be ignored
		self.size = (-1, -1)
	
	def __str__(self) -> str:
		"""
		Returns a printable representation of an Environment
		
		:return: printable representation of self
		"""
		
		return repr(self)
	
	def build(self, progressbar: bool = False) -> bool:
		"""
		Builds the full visibility graph based on the current graph
		
		This method removes any previously generated visibility elements!
		
		:return: success of the operation (hopefully True)
		"""
		
		# This list of vertices manages all "old" vertices that
		# lie "in front" of all following vertices and could possibly
		# block something, so they have to be checked for
		checker = []
		counter = 0
		
		# Enable the progressbar if requested
		if progressbar:
			progress = tqdm
		else:
			progress = progressAlternative
		
		# Style the progressbar (only useful when option -p is set)
		for v in progress(
			self.graph.get(Vertex),
			desc = "Progress",
			unit_scale = False,
			bar_format = "{l_bar}{bar}| {n_fmt}/{total_fmt} vertices"
		):
			log.debug("Builder:", v)
			
			# If it's the first vertex, it's always free
			if counter < 1:
				v.free = True
			
			# Otherwise, visibility has to be checked
			else:
				v.free = visibility.isVisible(v, Vertex(
					0,
					int(round(visibility.getYIntersection(v), 0))
				), self.graph)
			
			# Logging is always a great idea
			log.debug("Checking against", len(checker), "vertices...")
			
			# Check every open vertex against the current vertex
			for node in checker:
				visible = visibility.isVisible(v, node, self.graph)
				if not visible:
					self.graph.block((v, node))
			
			# Incrementing the counter and managing the list of
			# vertices left to be checked is important
			counter += 1
			checker.append(v)
		
		return True


def load(pathname: str) -> Environment:
	"""
	Loads the Environment object from a file
	
	Attention! This function is insecure as there're no checks
	built into it. It may be possible to execute malicious
	code or get broken data when using this feature!
	
	:param pathname: path to a valid pickle file that provides the Environment
	:return: Environment object read from that file
	"""
	
	log.info("Attempting to load Environment from file:", pathname)
	log.error("Loading Environments is currently not supported!")
	
	return Environment([])


def save(pathname: str, env: Environment) -> int:
	"""
	Saves the Environment object to a file
	
	:param pathname: path where to save the data (path may be overwritten!)
	:param env: Environment object that should be stored
	:return: number of bytes written to that file
	"""
	
	log.info("Attempting to save Environment to file:", pathname)
	log.error("Saving Environments is currently not supported!")
	
	return -1
